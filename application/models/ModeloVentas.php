<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVentas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getventa($idventa){
        $strq="SELECT v.ventaId,p.personalId,p.nombre as vendedor,c.Nombre as cliente, v.subtotal,v.ndescuento, v.descuento, v.total, v.tipopago,v.fechavencimiento,v.pagado,v.metodo,v.cancelado,v.canceladoh,v.reg,v.sucursalid
            FROM ventas as v
            inner join personal as p on p.personalId=v.personalId
            inner join clientes as c on c.ClientesId=v.ClientesId
            WHERE v.ventaId=$idventa";
        $resp=$this->db->query($strq);
        return $resp;
    }
    function getventad($idventa){
        $strq="SELECT vd.ventadId,pr.nombre, vd.cantidad,vd.precio,vd.precioc
            FROM venta_detalle as vd
            inner JOIN productos as pr on pr.productoid=vd.productoid
            WHERE vd.ventaId=$idventa";
        $resp=$this->db->query($strq);
        return $resp;
    }
}
