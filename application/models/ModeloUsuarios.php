<?php


defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloUsuarios extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function personal($sucursal){
        if ($sucursal==0) {
            $where='';
        }else{
            $where='and per.tipo=1 and per.sucursalId='.$sucursal;
        }
        $strq ="SELECT per.personalId,per.nombre,sucu.sucursal 
                FROM personal as per
                inner join sucursales as sucu on sucu.sucursalid=per.sucursalId
                where per.activo=1 $where";

        $query = $this->db->query($strq);
        return $query;

    }
    public function perfiles($sucursal){
        if ($sucursal==0) {
            $where='';
        }else{
            $where=' where perfilId!=1 ';
        }
        $strq ="SELECT * 
                FROM perfiles
                $where ";

        $query = $this->db->query($strq);
        return $query;
    }
    function usuarios($sucursal){
        if ($sucursal==0) {
            $where='';
        }else{
            $where=' ps.tipo=1 and ps.sucursalId='.$sucursal.' and';
        }
        $strq ="SELECT usu.UsuarioID,usu.perfilId,pf.nombre as perfil, usu.personalId,ps.nombre as personal,usu.Usuario,s.sucursal
                from usuarios as usu
                inner JOIN perfiles as pf on pf.perfilId=usu.perfilId
                inner JOIN personal as ps on ps.personalId=usu.personalId
                inner JOIN sucursales as s on s.sucursalid=ps.sucursalId
                WHERE $where ps.activo=1 AND usu.status=1 ";

        $query = $this->db->query($strq);
        return $query;

    }
    function numusuarios(){
        $strq = "SELECT count(*) as total FROM usuarios where status=1";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function numusuariospermitidos(){
        $strq = "SELECT usuariosmaximo FROM configuracion where id=1";
        $query = $this->db->query($strq);
        $usuariosmaximo=0;
        foreach ($query->result() as $row) {
            $usuariosmaximo =$row->usuariosmaximo;
        } 
        return $usuariosmaximo;
    }
}
