<?php
//ini_set("session.cookie_lifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia esta funcion no funciona en php7
//ini_set("session.gc_maxlifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia esta funcion no funciona en php7
/*
$a=session_id();
if(empty($a)) session_start();
*/
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function login($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,per.sucursalId,suc.logo
                 FROM usuarios as usu
                 inner join personal as per on per.personalId = usu.personalId
                 inner join sucursales as suc on suc.sucursalid = per.sucursalId
                 where usu.Usuario ='$usu'";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId;
            $sucursalId = $row->sucursalId;
            $logo = $row->logo; 
            
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid' => $id,
                        'usuario' => $nom,
                        'perfilid'=>$perfil,
                        'idpersonal'=>$idpersonal,
                        'sucursalId'=>$sucursalId,
                        'logo'=>$logo
                    );
                $this->session->set_userdata($data);
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        
        
        echo $count;
    }
    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        return $query;

    }

}
