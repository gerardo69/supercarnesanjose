<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$idname,$id){
        $this->db->set($data);
        $this->db->where($idname, $id);
        $this->db->update($Tabla);
        return $id;
    }
    public function deleteCatalogo($table,$idname,$id){
        $this->db->where($idname, $id);
        $this->db->delete($table);
    }
    function updatestock($Tabla,$value,$masmeno,$value2,$idname,$id){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id ";
        $query = $this->db->query($strq);
        return $id;
    }
    function updatestock2($Tabla,$value,$masmeno,$value2,$idname,$id,$idname2,$id2){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id and $idname=$id";
        $query = $this->db->query($strq);
        return $id;
    }
    function getselectvalue1rowwhere($table,$colw,$valw){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colw,$valw);
        $query=$this->db->get(); 
        return $query;
    }
    function getselectvalue1rowwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function getultimosproductos(){
        $strq = "SELECT * FROM productos where activo=1 ORDER BY productoid DESC LIMIT 5";
        $query = $this->db->query($strq);
        return $query;
    }
    function getproductosstocktotal(){
        $strq = "SELECT 
                        sum(stock1) as stock1,
                        sum(stock2) as stock2,
                        sum(stock3) as stock3,
                        COUNT(*) as total 
                        from productos 
                        where activo=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function getultimasventas($sucursal){
        if ($sucursal==0) {
            $winner='';
            $where='';
        }else{
            $winner=' inner JOIN personal as per on per.personalId=v.personalId ';
            $where=' and per.sucursalId='.$sucursal;
        }

        $strq = "SELECT v.ventaId,cli.Nombre ,v.total, v.reg 
                FROM ventas as v 
                inner JOIN clientes as cli on cli.ClientesId=v.ClientesId
                $winner
                WHERE v.pagado=1 $where
                ORDER BY v.ventaId DESC LIMIT 3";
        $query = $this->db->query($strq);
        return $query;
    }
    function gettotaldeventas($perfil,$sucursal){
        $year=date('Y');
        if ($perfil>1) {
            $where ='';
            $innerjoin='';
        }else{
            $innerjoin=' inner join personal as pe on pe.personalId=ve.personalId ';
            $where =' and pe.sucursalId='.$sucursal;
        }
        $strq = "SELECT sum(ve.total) as total 
                from ventas as ve 
                $innerjoin
                WHERE ve.pagado=1 and ve.cancelado=0 AND ve.reg>='$year-01-01 00:00:00' $where";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    function gettotaldecompras($perfil,$sucursal){
        $year=date('Y');
        if ($perfil>1) {
            $where ='';
            $innerjoin='';
        }else{
            $innerjoin=' inner join personal as pe on pe.personalId=com.personalId ';
            $where =' and pe.sucursalId='.$sucursal;
        }
        $strq = "SELECT sum(com.monto_total) as total,count(*) as reg 
                from compras as com 
                $innerjoin
                WHERE com.reg>='$year-01-01 00:00:00' $where";
        $query = $this->db->query($strq);
        $total=0;
        $reg=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
            $reg=$row->reg;
        }
        $compras = array('total' => $total,'reg'=>$reg );
        return $compras; 
    }
    function gettotalclientes(){
        $strq = "SELECT COUNT(*) as total FROM clientes WHERE activo=1";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    public function getselectwherelike2($tables,$where,$likec1,$likev1,$likec2,$likev2){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($likec1,$likev1, 'both'); 
        $this->db->or_like($likec2,$likev2, 'both');  
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwherelike1($tables,$where,$likec1,$likev1){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($likec1,$likev1, 'both'); 
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    function tickeall(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        return $query;
    }
    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=$perfil AND MenusubId=$modulo";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    function codebar($id){
        $strq = "SELECT codigo FROM `ticket_bar` WHERE barId=$id";
        $query = $this->db->query($strq);
        $codigo=0;
        foreach ($query->result() as $row) {
            $codigo=$row->codigo;
        }
        return $codigo; 
    }
    //================ listado personal ===========================
        function List_table_personal($params,$sucursal){
            $columns = array( 
                0=>'p.personalId',
                1=>'p.nombre',
                2=>'p.celular',
                3=>'s.sucursal',
                4=>'p.correo'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('personal p');
            $this->db->join('sucursales s', 's.sucursalid=p.sucursalId');
            if ($sucursal==0) {
                $where = array('p.activo'=>1);
            }else{
                $where = array('p.activo'=>1,'p.sucursalId'=>$sucursal);
            }
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function filastotal_personal($params,$sucursal){
            $columns = array( 
                0=>'p.personalId',
                1=>'p.nombre',
                2=>'p.celular',
                3=>'s.sucursal',
                4=>'p.correo'
            );
            $this->db->select('COUNT(1) as total');
            $this->db->from('personal p');
            $this->db->join('sucursales s', 's.sucursalid=p.sucursalId');
            if ($sucursal==0) {
                $where = array('p.activo'=>1);
            }else{
                $where = array('p.activo'=>1,'p.sucursalId'=>$sucursal);
            }
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();   
            }
            $query=$this->db->get();
            return $query->row()->total;
        }
    //================ listado personal fin ===========================
    //================ listado categoria ===========================
        function filastotal_categoria($buscar){
            if ($buscar=='') {
                $where='activo=1';
            }else{
                $whereb=" activo=1";
                $where= $whereb." and categoria like '%".$buscar."%'";
            }
            $strq = "SELECT COUNT(*) as total 
                    FROM categoria 
                    where $where";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        function List_table_categoria($por_pagina,$segmento,$buscar){
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            if ($buscar=='') {
                $where='cat.activo=1';
            }else{
                $whereb=" cat.activo=1";
                $where= $whereb." and cat.categoria like '%".$buscar."%' ";
            }
            $strq="SELECT cat.categoriaId,cat.categoria,cat.activo,cat.reg,
                    (
                        SELECT count(*) as total from productos as pro where pro.categoria=cat.categoriaId
                    ) as total
                    FROM categoria as cat 
                    WHERE $where LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ listado personal fin ===========================
    //================ listado productos ===========================
        function filastotal_productos(){
            $strq = "SELECT COUNT(*) as total 
                    FROM  productos as pro
                    where pro.activo=1";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        function List_table_productos_asi($params){
            $sucursal=$params['sucu'];
            $columns = array( 
                0=>'pro.productoid',
                1=>'pro.img',
                2=>'pro.codigo',
                3=>'pro.nombre',
                4=>'pro.descripcion',
                5=>'cat.categoria',
                6=>'pros.existencia',
                7=>'pros.precio_venta',
                 
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('productos pro');
            $this->db->join('categoria cat', 'cat.categoriaId=pro.categoria');
            $this->db->join('productos_sucursales pros', 'pros.idproducto=pro.productoid','left');
            $where = array(
                'pro.activo'=>1,
                'pros.idsucursal'=>$sucursal
            );
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }

    //================ listado producto fin ===========================
    //================ listado clientes ===========================
        function List_table_clientes($params){
            $columns = array( 
                0=>'ClientesId',
                1=>'Nombre',
                2=>'Domicilio',
                3=>'Correo',
                4=>'nombrec',
                5=>'correoc',
                6=>'telefonoc'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('clientes');
            $where = array('activo'=>1);
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function filastotal_clientes($params){
            $columns = array( 
                0=>'ClientesId',
                1=>'Nombre',
                2=>'Domicilio',
                3=>'Correo',
                4=>'nombrec',
                5=>'correoc',
                6=>'telefonoc'
            );
            $this->db->select('COUNT(1) as total');
            $this->db->from('clientes');
            $where = array('activo'=>1);
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();   
            }
            $query=$this->db->get();
            return $query->row()->total;
        }
    //================ listado clientes fin ===========================
    //================ listado proveedores ===========================    
        function List_table_proveedores($params){
            $columns = array( 
                0=>'id_proveedor',
                1=>'razon_social',
                2=>'domicilio',
                3=>'ciudad',
                4=>'telefono_local',
                5=>'telefono_celular',
                6=>'email_contacto',
                7=>'rfc'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('proveedores');
            $where = array('activo'=>1);
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function filastotal_proveedores($params){
            $columns = array( 
                0=>'id_proveedor',
                1=>'razon_social',
                2=>'domicilio',
                3=>'ciudad',
                4=>'telefono_local',
                5=>'telefono_celular',
                6=>'email_contacto',
                7=>'rfc'
            );
            $this->db->select('COUNT(1) as total');
            $this->db->from('proveedores');
            $where = array('activo'=>1);
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();   
            }
            $query=$this->db->get();
            return $query->row()->total;
        }
    //================ listado proveedores fin ===========================    
    //================ Verifcar Producto ===================
        function List_table_verificar($buscar,$sucursal){
            $strq="SELECT pro.productoid,pro.img,pro.codigo,pro.nombre,pros.precio_venta
                    FROM productos as pro
                    inner join productos_sucursales as pros on pros.idproducto=pro.productoid
                    WHERE 
                    pro.activo=1 AND 
                    pros.idsucursal=$sucursal and 
                    pro.codigo ='$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ Verificar Producto fin ===========    
    //================ Gastos ===========
        function List_table_gastos($params,$sucursal){
            $columns = array( 
                0=>'g.gastosid',
                1=>'g.fecha',
                2=>'g.cantidad',
                3=>'g.concepto',
                4=>'p.nombre',
                5=>'s.sucursal',
                6=>'g.activo',
                7=>'pc.nombre as personalc',
                8=>'g.motivo_cancela',
                9=>'g.reg_cancela'
            );
            $columns2 = array( 
                0=>'g.gastosid',
                1=>'g.fecha',
                2=>'g.cantidad',
                3=>'g.concepto',
                4=>'p.nombre',
                5=>'s.sucursal',
                6=>'g.activo',
                7=>'pc.nombre',
                8=>'g.motivo_cancela',
                9=>'g.reg_cancela'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('gastos g');
            $this->db->join('personal p', 'p.personalId=g.personalId');
            $this->db->join('sucursales s', 's.sucursalid=g.sucursal');
            $this->db->join('personal pc', 'pc.personalId=g.personalId_cancela','left');
            if ($sucursal!=0) {
                $where = array('p.sucursalId'=>$sucursal);
                $this->db->where($where);
            }            
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns2 as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function filastotal_gastos($params,$sucursal){
            $columns = array( 
                0=>'g.gastosid',
                1=>'g.fecha',
                2=>'g.cantidad',
                3=>'g.concepto',
                4=>'p.nombre',
                5=>'s.sucursal',
                6=>'g.activo',
                7=>'pc.nombre',
                8=>'g.motivo_cancela',
                9=>'g.reg_cancela'
            );
            $this->db->select('COUNT(1) as total');
            $this->db->from('gastos g');
            $this->db->join('personal p', 'p.personalId=g.personalId');
            $this->db->join('sucursales s', 's.sucursalid=g.sucursal');
            $this->db->join('personal pc', 'pc.personalId=g.personalId_cancela','left');
            if ($sucursal!=0) {
                $where = array('p.sucursalId'=>$sucursal);
                $this->db->where($where);
            }  
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();   
            }
            $query=$this->db->get();
            return $query->row()->total;
        }
    //================ Gastos fin ===========        
    //================ Sucursal ===========
        function List_table_sucursal($params){
            $columns = array( 
                0=>'sucursalid',
                1=>'logo',
                2=>'sucursal',
                3=>'direccion',
                4=>'telefono'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('sucursales');
            $where = array('activo'=>1);
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function filastotal_sucursal($params){
            $columns = array( 
                0=>'sucursalid',
                1=>'logo',
                2=>'sucursal',
                3=>'direccion',
                4=>'telefono'
            );
            $this->db->select('COUNT(1) as total');
            $this->db->from('sucursales');
            $where = array('activo'=>1);
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();   
            }
            $query=$this->db->get();
            return $query->row()->total;
        }
    //================ Sucursal fin ===========  
    //================ Listado Ventas normales===========
        function List_table_ventasnormales($params,$sucursal){
            $columns = array( 
                0=>'v.ventaId',
                1=>'c.Nombre as cliente',
                2=>'v.subtotal',
                3=>'v.ndescuento',
                4=>'v.descuento',
                5=>'v.total',
                6=>'v.reg',
                7=>'v.cancelado',
                8=>'pc.nombre',
                9=>'v.cancela_motivo',
                10=>'v.canceladoh'
            );
            $columns2 = array( 
                0=>'v.ventaId',
                1=>'c.Nombre',
                2=>'v.subtotal',
                3=>'v.ndescuento',
                4=>'v.descuento',
                5=>'v.total',
                6=>'v.reg',
                7=>'v.cancelado',
                8=>'pc.nombre',
                9=>'v.cancela_motivo',
                10=>'v.canceladoh'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas v');
            $this->db->join('clientes c', 'c.ClientesId = v.ClientesId','left');
            $this->db->join('personal pc', 'pc.personalId = v.personalId','left');
            if ($sucursal==0) {
                $where = array('v.tipopago'=>1);
            }else{
                $where = array('v.tipopago'=>1,'v.sucursalId'=>$sucursal);
            }  
            $this->db->where($where);        
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns2 as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function filastotal_ventasnormales($params,$sucursal){
            $columns = array( 
                0=>'v.ventaId',
                1=>'c.Nombre',
                2=>'v.subtotal',
                3=>'v.ndescuento',
                4=>'v.descuento',
                5=>'v.total',
                6=>'v.reg',
                7=>'v.cancelado',
                8=>'pc.nombre',
                9=>'v.cancela_motivo',
                10=>'v.canceladoh'
            );
            $this->db->select('COUNT(1) as total');
            $this->db->from('ventas v');
            $this->db->join('clientes c', 'c.ClientesId = v.ClientesId','left');
            $this->db->join('personal pc', 'pc.personalId = v.personalId','left');
            if ($sucursal==0) {
                $where = array('v.tipopago'=>1);
            }else{
                $where = array('v.tipopago'=>1,'v.sucursalId'=>$sucursal);
            }  
            $this->db->where($where);        
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();   
            }
            $this->db->order_by('v.reg', 'DESC');
            $query=$this->db->get();
            return $query->row()->total;
        } 
    //================ Listado Ventas normales fin ===========  
    //================ Listado Ventas Credito =============
        function List_table_ventascredito($params,$sucursal){
            $columns = array( 
                0=>'v.ventaId',
                1=>'c.Nombre as cliente',
                2=>'v.subtotal',
                3=>'v.ndescuento',
                4=>'v.descuento',
                5=>'v.total',
                6=>'v.fechavencimiento',
                7=>'v.cancelado',
                8=>'v.pagado',
                9=>'p.nombre',
                10=>'v.cancela_motivo',
                11=>'v.canceladoh'
            );
            $columns2 = array( 
                0=>'v.ventaId',
                1=>'c.Nombre',
                2=>'v.subtotal',
                3=>'v.ndescuento',
                4=>'v.descuento',
                5=>'v.total',
                6=>'v.fechavencimiento',
                7=>'v.cancelado',
                8=>'v.pagado',
                9=>'p.nombre',
                10=>'v.cancela_motivo',
                11=>'v.canceladoh'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas v');
            $this->db->join('clientes c', 'c.ClientesId = v.ClientesId','left');
            $this->db->join('personal p', 'p.personalId = v.cancela_personal','left');
            if ($sucursal==0) {
                $where = array('v.tipopago'=>2);
            }else{
                $where = array('v.tipopago'=>2,'v.sucursalId'=>$sucursal);
            }  
            $this->db->where($where);       
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns2 as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function filastotal_ventascredito($params,$sucursal){
            $columns = array( 
                0=>'v.ventaId',
                1=>'c.Nombre',
                2=>'v.subtotal',
                3=>'v.ndescuento',
                4=>'v.descuento',
                5=>'v.total',
                6=>'v.fechavencimiento',
                7=>'v.cancelado',
                8=>'v.pagado',
                9=>'p.nombre',
                10=>'v.cancela_motivo',
                11=>'v.canceladoh'
            );
            $this->db->select('COUNT(1) as total');
            $this->db->from('ventas v');
            $this->db->join('clientes c', 'c.ClientesId = v.ClientesId','left');
            $this->db->join('personal p', 'p.personalId = v.cancela_personal','left');
            if ($sucursal==0) {
                $where = array('v.tipopago'=>2);
            }else{
                $where = array('v.tipopago'=>2,'v.sucursalId'=>$sucursal);
            }  
            $this->db->where($where);   
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();   
            }
            $this->db->order_by('v.reg', 'DESC');
            $query=$this->db->get();
            return $query->row()->total;
        } 
    //================ Listado Ventas Credito fin ===========      
    //================ Listado pagos ===========
        function List_table_pagos($buscar){
            $strq="SELECT  pagoId, pago, reg
                   FROM pagos_credito
                   WHERE ventaId ='$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
        }
  
        function verificarcantidad($id){
            $strq="SELECT total FROM  ventas WHERE ventaId ='$id'";
            $resp=$this->db->query($strq);
            $total=0;
            foreach ($resp->result() as $row) {
                $total =$row->total;
                }
            return $total;
        }
        function verificarpago($buscar){
            $strq="SELECT SUM(p.pago) as suma, v.total FROM pagos_credito AS p
                   LEFT JOIN ventas as v ON  v.ventaId = p.ventaId
                   WHERE p.ventaId = '$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
        }

    //================ Listado pagos fin =========== 
    //================ Listado compras ===========
        function List_table_compras($params,$sucursal){
            $columns = array( 
                0=>'co.compraId',
                1=>'co.monto_total',
                2=>'co.reg',
                3=>'pr.razon_social',
                4=>'per.nombre'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('compras co');
            $this->db->join('proveedores pr', 'pr.id_proveedor=co.id_proveedor');
            $this->db->join('personal per', 'per.personalId=co.personalId');
            if ($sucursal==0) {
                $where = array('co.activo'=>1);
            }else{
                $where = array('co.activo'=>1,'co.sucursalId'=>$sucursal);
            }
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function filastotal_compras($params,$sucursal){
            $columns = array( 
                0=>'co.compraId',
                1=>'co.monto_total',
                2=>'co.reg',
                3=>'pr.razon_social',
                4=>'per.nombre'
            );
            $this->db->select('COUNT(1) as total');
            $this->db->from('compras co');
            $this->db->join('proveedores pr', 'pr.id_proveedor=co.id_proveedor');
            $this->db->join('personal per', 'per.personalId=co.personalId');
            if ($sucursal==0) {
                $where = array('co.activo'=>1);
            }else{
                $where = array('co.activo'=>1,'co.sucursalId'=>$sucursal);
            }
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();   
            }
            $query=$this->db->get();
            return $query->row()->total;
        }
    //================ Listado compras Fin===========
    //================ Modificacion del stoct1 ===========
    function getventasdestalle($buscar){
            $strq="SELECT vd.cantidad, p.nombre ,vd.productoid FROM `ventas` AS v 
                    INNER JOIN venta_detalle AS vd ON vd.ventaId = v.ventaId
                    INNER JOIN productos AS p ON p.productoid = vd.productoid
                   WHERE v.ventaId ='$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
            }
    function getproducto($buscar){
            $strq="SELECT productoid, stock1, productoid  FROM productos
                   WHERE productoid ='$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
            }        
    //================ Modificacion del stoct1 Fin ===========    
    function getbackup(){
        $strq="SELECT bac.backupid,bac.name,bac.reg,per.nombre FROM backup as bac inner JOIN personal as per on per.personalId=bac.personalId ORDER BY `bac`.`backupid` DESC limit 10";
            $resp=$this->db->query($strq);
            return $resp;
    }    
    function compraproductos($id){
        $strq="SELECT cod.cantidad,pro.nombre,cod.precio_compra 
                FROM compras_detalles as cod
                inner join productos as pro on pro.productoid=cod.productoid
                WHERE cod.compraId=$id";
            $resp=$this->db->query($strq);
            return $resp;
    }
    function getdatoscompras($idcompra){
        $strq="SELECT com.compraId, prov.razon_social,com.sucursalid,com.monto_total,com.reg
            FROM compras as com
            inner join proveedores as prov on prov.id_proveedor=com.id_proveedor
            WHERE com.compraId=$idcompra";
        $resp=$this->db->query($strq);
            return $resp;
    }
    function getdatoscomprasd($idcompra){
        $strq="SELECT comd.cantidad,pro.nombre,comd.precio_compra 
                FROM compras_detalles as comd
                inner join productos as pro on pro.productoid=comd.productoid
                WHERE comd.compraId=$idcompra";
        $resp=$this->db->query($strq);
            return $resp;
    }
}
