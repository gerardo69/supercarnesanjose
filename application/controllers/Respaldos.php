<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Respaldos extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->personal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,21);// 21 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index(){
        $data['total_rows'] = $this->ModeloCatalogos->getbackup();
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/respaldos',$data);
        $this->load->view('templates/footer');
        $this->load->view('config/categoriajs');
        $this->load->view('config/respaldosjs');
        
	}
    function generar(){
        
        $this->load->dbutil();
        $namefile='Backup_pos_' . date('Y-m-d_H-i-s');
        $prefs = array(
            'tables'    => array(
                            'categoria', 
                            'clientes',
                            'compras',
                            'compras_detalles',
                            'configuracion',
                            'gastos',
                            'pagos_credito',
                            'personal',
                            'productos',
                            'proveedores',
                            'sucursales',
                            'ticket',
                            'usuarios',
                            'ventas',
                            'venta_detalle'
                        ),
            'format'    => 'zip', 
            'filename'  => $namefile
        );
            
        $backup = $this->dbutil->backup($prefs);
        //if (!write_file('./uploads/backup/' . $namefile . '.zip', $backup)) { se quito la extencion para que no sepan el tipo de archivo
        if (!write_file('./uploads/backup/' . $namefile . '', $backup)) {

            $msj=0;
            $title='Error!';
            $text='Error en generar respaldo';
            $type='error';
        }else {
            $msj=1;
            $title='Hecho!';
            $text='Respaldo generado correctamente';
            $type='success';
            //$this->load->helper('download');
            //force_download($namefile, $backup); 
            $data['name']=$namefile;
            $data['personalId']=$this->personal;
            $this->ModeloCatalogos->Insert('backup',$data);

        }
        $respuesta=array(
                'msj' =>$msj,
                'title' =>$title,
                'text' =>$text,
                'type' =>$type
            );

        echo json_encode($respuesta);
    }
    

    

}