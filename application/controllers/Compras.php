<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->personal=$this->session->userdata('idpersonal');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->sucursalIdd=$this->sucursalId;
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,5);// 6 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compraslista');
        $this->load->view('templates/footer');
        $this->load->view('compras/compraslistajs');
	}
    function Comprasadd(){
        $data['sucursalesrow']=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','activo',1);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compraadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('compras/compraaddjs');

    }
    function ingresarcompra(){
        $data = $this->input->post();
        $data['personalId']=$this->personal;
        $data['reg']=$this->fechahoy;

        $id=$this->ModeloCatalogos->Insert('compras',$data);
        echo $id;
    }
    function ingresarcomprapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $data['compraId'] = $DATA[$i]->compraId;
            $productoid=$DATA[$i]->productoid;
            $data['productoid'] = $productoid;
            $cantidad=$DATA[$i]->cantidad;
            $data['cantidad'] = $DATA[$i]->cantidad;
            $precio_compra=$DATA[$i]->precio_compra;
            $data['precio_compra'] = $precio_compra;

            $id=$this->ModeloCatalogos->Insert('compras_detalles',$data);

            $datapro = array('preciocompra'=>$precio_compra);
            $this->ModeloCatalogos->updateCatalogo('productos',$datapro,'productoid',$productoid);
            /*
            $stock='stock'.$this->sucursalIdd;
            $this->ModeloCatalogos->updatestock('productos',$stock,'+',$cantidad,'productoid',$productoid);
            */
            $this->ModeloCatalogos->updatestock2('productos_sucursales','existencia','+',$cantidad,'idproducto',$productoid,'idsucursal',$DATA[$i]->sucursalid);
        }
    }
    function compraproductos(){
        $id = $this->input->post('compraid');
        $result=$this->ModeloCatalogos->compraproductos($id);
        $html='<table class="table table-striped jambo_table bulk_action" id="tableproductoscompras">
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Producto</th>
                        <th>Precio Compra</th>
                    </tr>
                <thead>
                <tbody>
                ';
        foreach ($result->result() as $item) {
            $html.='<tr>
                        <td>'.$item->cantidad.'</td>
                        <td>'.$item->nombre.'</td>
                        <td>'.$item->precio_compra.'</td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    public function getData_listado(){
        $params = $this->input->post();
        $productos = $this->ModeloCatalogos->List_table_compras($params,$this->sucursalId);
        $totalRecords=$this->ModeloCatalogos->filastotal_compras($params,$this->sucursalId); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    

}