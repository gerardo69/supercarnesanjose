<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
	public function __construct(){
        parent::__construct();
        //$this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloReportes');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,3);// 3 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index(){
        $data["sucursalId"]=$this->sucursalId;
        $data["perfilid"]=$this->perfilid;
        $data['stock2activo']=$this->ModeloReportes->getsucursalactiva(2);
        $data['stock3activo']=$this->ModeloReportes->getsucursalactiva(3);

        $data['sucursalesrow']=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','activo',1);
        if ($this->perfilid==1) {
            $data['list_block']='';
        }else{
            $data['list_block']='style="display: none;"';
        }
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productoslist',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/productosljs');    
	}
    function Productosadd($id=0){
        //$id = $this->uri->segment(3);
        //echo $id;
        $data['perfilid'] = $this->perfilid;
        $data['sucursalId'] = $this->sucursalId;

        $data['sucursalesrow']=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','activo',1);
        $data['categorias']=$this->ModeloCatalogos->getselectvalue1rowwhere('categoria','activo',1);
        if ($id==0) {
            $data['subtitle']='Nuevo';
            $data['id']=$id;
            $data['button']='Guardar';
            $data['productoid']=$id;
            $data['codigo']='';
            $data['nombre']='';
            $data['descripcion']='';
            $data['categoria']='';
            $data['stock1']='';
            $data['stock2']='';
            $data['stock3']='';
            $data['preciocompra']='';
            $data['precioventa']='';

            $data['img']='';
        }else{
            $result=$this->ModeloCatalogos->getselectvalue1rowwhere('productos','productoid',$id);
            foreach ($result->result() as $row) {
                $data['productoid']=$row->productoid;
                $data['codigo']=$row->codigo;
                $data['nombre']=$row->nombre;
                $data['descripcion']=$row->descripcion;
                $data['categoria']=$row->categoria;
                $data['stock1']=$row->stock1;
                $data['stock2']=$row->stock1;
                $data['stock3']=$row->stock1;
                $data['preciocompra']=$row->preciocompra;
                $data['precioventa']=$row->precioventa;
                $data['img']=$row->img;
            }
            $data['subtitle']='Editar';
            $data['id']=$id;
            $data['button']='Actualizar';
            
        }
        

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productosadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/productosaddjs',$data);
    }
    function add(){
        $this->load->library('upload');
        $data = $this->input->post();
        //echo $data['productoid'];
        if (isset($_FILES['img'])) {
            //===========================================================================
                $target_path = 'public/images/productos';
                $thumb_path = 'public/images/productost';
                //file name setup
                $filename_err = explode(".",$_FILES['img']['name']);
                $filename_err_count = count($filename_err);
                $file_ext = $filename_err[$filename_err_count-1];
                //if($file_name != ''){
                   // $fileName = $file_name.'.'.$file_ext;
                //}else{
                    $fileName = $_FILES['img']['name'];
                //}
                $fecha=date('ymd-His');
                //upload image path
                $cargaimagen =$fecha.'cat'.basename($fileName);
                $upload_image = $target_path.'/'.$cargaimagen;
                
                //upload image
                if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
                    $data['img']=$cargaimagen;
                    //thumbnail creation
                        //$this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                        $thumbnail = $thumb_path.'/'.$cargaimagen;
                        list($width,$height) = getimagesize($upload_image);
                        if ($width>3000) {
                            $thumb_width = $width/10;
                            $thumb_height = $height/10;
                        }elseif ($width>2500) {
                            $thumb_width = $width/7.9;
                            $thumb_height = $height/7.9;
                        }elseif ($width>2000) {
                            $thumb_width = $width/6.8;
                            $thumb_height = $height/6.8;
                        }elseif ($width>1500) {
                            $thumb_width = $width/5.1;
                            $thumb_height = $height/5.1;
                        }elseif ($width>1000) {
                            $thumb_width = $width/3.3;
                            $thumb_height = $height/3.3;
                        }elseif ($width>900) {
                            $thumb_width = $width/3;
                            $thumb_height = $height/3;
                        }elseif ($width>800) {
                            $thumb_width = $width/2.6;
                            $thumb_height = $height/2.6;
                        }elseif ($width>700) {
                            $thumb_width = $width/2.3;
                            $thumb_height = $height/2.3;
                        }elseif ($width>600) {
                            $thumb_width = $width/2;
                            $thumb_height = $height/2;
                        }elseif ($width>500) {
                            $thumb_width = $width/1.9;
                            $thumb_height = $height/1.9;
                        }elseif ($width>400) {
                            $thumb_width = $width/1.3;
                            $thumb_height = $height/1.3;
                        }elseif ($width>300) {
                            $thumb_width = $width/1.2;
                            $thumb_height = $height/1.2;
                        }else{
                            $thumb_width = $width;
                            $thumb_height = $height;
                        }
                        $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                        switch($file_ext){
                            case 'jpg':
                                $source = imagecreatefromjpeg($upload_image);
                                break;
                            case 'jpeg':
                                $source = imagecreatefromjpeg($upload_image);
                                break;

                            case 'png':
                                $source = imagecreatefrompng($upload_image);
                                break;
                            case 'gif':
                                $source = imagecreatefromgif($upload_image);
                                break;
                            default:
                                $source = imagecreatefromjpeg($upload_image);
                        }

                        imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                        switch($file_ext){
                            case 'jpg' || 'jpeg':
                                imagejpeg($thumb_create,$thumbnail,100);
                                break;
                            case 'png':
                                imagepng($thumb_create,$thumbnail,100);
                                break;

                            case 'gif':
                                imagegif($thumb_create,$thumbnail,100);
                                break;
                            default:
                                imagejpeg($thumb_create,$thumbnail,100);
                        }

                    

                    $return = Array('ok'=>TRUE,'img'=>'');
                }
                else{
                    $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                }
            //===========================================================================
            

            //unset($data['img']);
        }
        $id=$data['productoid'];
        $stock=$data['stock'];
        unset($data['stock']);
        unset($data['productoid']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('productos',$data,'productoid',$id);
            $result=1;
        }else{
            //echo $data;
            $tpro=$this->ModeloProductos->numproductos();
            $tpropermitidos=$this->ModeloProductos->numproductospermitidos();
            if ($tpro<$tpropermitidos) {
                $id=$this->ModeloCatalogos->Insert('productos',$data);
                $result=1;
            }else{
                $result=2;
            } 
        }
        if ($result==1) {
            //log_message('error', 'entra');
            //log_message('error', 'stock:'.$stock);
            $STOCK = json_decode($stock);
            //log_message('error', 'stock2:'.$STOCK);
            for ($i=0;$i<count($STOCK);$i++) { 
                $idproductosucursal = $STOCK[$i]->idproductosucursal;
                $datasp['idproducto'] = $id;

                $datasp['idsucursal'] = $STOCK[$i]->idsucursal;
                $datasp['precio_venta'] = $STOCK[$i]->precioventa;
                $datasp['mayoreo'] = $STOCK[$i]->mayoreo;
                $datasp['can_mayoreo'] = $STOCK[$i]->can_mayoreo;
                $datasp['existencia'] = $STOCK[$i]->existencia;
                if ($idproductosucursal>0) {
                    $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$datasp,'id',$idproductosucursal);
                }else{
                    $this->ModeloCatalogos->Insert('productos_sucursales',$datasp);
                }

            }
        }
        echo $result;
    }
    public function eliminar(){
        $id = $this->input->post('productoid');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo('productos',$data,'productoid',$id);
    }
    public function getData_producto() {

        $params = $this->input->post();

        $productos = $this->ModeloCatalogos->List_table_productos_asi($params);

        $totalRecords=$this->ModeloCatalogos->filastotal_productos(); 

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }

}