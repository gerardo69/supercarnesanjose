<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        if ($this->session->userdata('logeado')) {
           //$this->propietarioid // aqui ira el id del propietarioid( nuestro cliente) que ingreso

            
        }else{
            redirect('Login');
        }
    }
	public function index(){
        //redirect('/Sistema');
	}
    public function searchproducto(){
        $search = $this->input->get('search');
        //$where =  array('activo' => 1,'propietarioid' => $this->propietarioid ); // se activara la funcionalidad de usuarios multiples
        $where =  array('activo' => 1 );
        //$results=$this->ModeloCatalogos->getselectwherelike('productos',$where,'nombre',$search);
        $results=$this->ModeloCatalogos->getselectwherelike2('productos',$where,'codigo',$search,'nombre',$search);
        echo json_encode($results);
    }
    function searchcliente(){
        $search = $this->input->get('search');
        //$where =  array('activo' => 1,'propietarioid' => $this->propietarioid ); // se activara la funcionalidad de usuarios multiples
        $where =  array('activo' => 1 );
        $results=$this->ModeloCatalogos->getselectwherelike1('clientes',$where,'Nombre',$search);
        echo json_encode($results);
    }
    function searchproveedor(){
        $search = $this->input->get('search');
        //$where =  array('activo' => 1,'propietarioid' => $this->propietarioid ); // se activara la funcionalidad de usuarios multiples
        $where =  array('activo' => 1 );
        $results=$this->ModeloCatalogos->getselectwherelike1('proveedores',$where,'razon_social',$search);
        echo json_encode($results);
    }
    function barproducto(){
        $codigo = $this->input->post('prod');
        $where = array('codigo' => $codigo,'activo'=>1);
        $results=$this->ModeloCatalogos->getselectvalue1rowwheren('productos',$where);
        $pro=0;
        $proid=0;
        $prorow=0;
        foreach ($results->result() as $item){
            $pro=1;
            $proid=$item->productoid;
            $prorow++;
        }
        $json_data = array(
            "pro"=>$pro,
            "proid"=>$proid,
            "prorow"=>$prorow 
        );
        
        echo json_encode($json_data);
    }
}
