<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListadoVentasNormales extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->personal=$this->session->userdata('idpersonal');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,17);// 17 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }

	public function index(){  
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('listadoventas/normales');
        $this->load->view('templates/footer');
        $this->load->view('listadoventas/jsnormales');
	}

    function cancelar(){
        $ventaId = $this->input->post('ventaId');
        $cancela_motivo = $this->input->post('cancela_motivo');
        $sumastock = $this->ModeloCatalogos->getventasdestalle($ventaId);
        foreach ($sumastock->result() as $item) {
            $cantidad = $item->cantidad;
            $idproducto = $item->productoid;
            $productos = $this->ModeloCatalogos->getproducto($idproducto);
            foreach ($productos->result() as $item) {
                $stock1 = $item->stock1; 
            }
            $suma = $cantidad + $stock1;
            $arraystock1 = array('stock1' => $suma);
            $result = $this->ModeloCatalogos->updateCatalogo('productos',$arraystock1,'productoid',$idproducto);
        }
        $data = array('cancelado'=>1,'cancela_personal'=> $this->personal,'cancela_motivo' => $cancela_motivo,'canceladoh'=> $this->fechahoy );
        $result = $this->ModeloCatalogos->updateCatalogo('ventas',$data,'ventaId',$ventaId);
    }
    public function getData_listado(){
        $params = $this->input->post();
        $productos = $this->ModeloCatalogos->List_table_ventasnormales($params,$this->sucursalId);
        $totalRecords=$this->ModeloCatalogos->filastotal_ventasnormales($params,$this->sucursalId); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
}