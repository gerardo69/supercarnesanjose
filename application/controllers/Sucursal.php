<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursal extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,16);// 3 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index(){
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('sucursal/sucursallist');
        $this->load->view('templates/footer');
        $this->load->view('sucursal/listsucursaljs');
	}
	public function Sucursaladd($id=0){
        if ($id==0) {
            $data['subtitle']='Nuevo';
            $data['button']='Guardar';
      
            $data['sucursalid'] = $id;
            $data['logo'] = '';
            $data['sucursal'] = '';
            $data['direccion'] = '';
            $data['telefono'] = '';
        }else{
            $result=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','sucursalid',$id);
            foreach ($result->result() as $row) {
                $data['sucursalid'] = $row->sucursalid;
                $data['logo'] = $row->logo;
                $data['sucursal'] = $row->sucursal;
                $data['direccion'] = $row->direccion;
                $data['telefono'] = $row->telefono;
            }
            $data['subtitle']='Editar';
            $data['button']='Actualizar';
            
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('sucursal/sucursal',$data);
        $this->load->view('templates/footer');
        $this->load->view('sucursal/jssucursal');
	}
    function add(){
        $this->load->library('upload');
        $data = $this->input->post();
        //echo $data['productoid'];
        if (isset($_FILES['logo'])) {
            //===========================================================================
                $target_path = 'public/images/sucursal';
                $thumb_path = 'public/images/sucursalt';
                //file name setup
                $filename_err = explode(".",$_FILES['logo']['name']);
                $filename_err_count = count($filename_err);
                $file_ext = $filename_err[$filename_err_count-1];
                //if($file_name != ''){
                   // $fileName = $file_name.'.'.$file_ext;
                //}else{
                    $fileName = $_FILES['logo']['name'];
                //}
                $fecha=date('ymd-His');
                //upload image path
                $cargaimagen =$fecha.'cat'.basename($fileName);
                $upload_image = $target_path.'/'.$cargaimagen;
                
                //upload image
                if(move_uploaded_file($_FILES['logo']['tmp_name'],$upload_image)){
                    $data['logo']=$cargaimagen;
                    //thumbnail creation
                        //$this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                        $thumbnail = $thumb_path.'/'.$cargaimagen;
                        list($width,$height) = getimagesize($upload_image);
                        if ($width>3000) {
                            $thumb_width = $width/10;
                            $thumb_height = $height/10;
                        }elseif ($width>2500) {
                            $thumb_width = $width/7.9;
                            $thumb_height = $height/7.9;
                        }elseif ($width>2000) {
                            $thumb_width = $width/6.8;
                            $thumb_height = $height/6.8;
                        }elseif ($width>1500) {
                            $thumb_width = $width/5.1;
                            $thumb_height = $height/5.1;
                        }elseif ($width>1000) {
                            $thumb_width = $width/3.3;
                            $thumb_height = $height/3.3;
                        }elseif ($width>900) {
                            $thumb_width = $width/3;
                            $thumb_height = $height/3;
                        }elseif ($width>800) {
                            $thumb_width = $width/2.6;
                            $thumb_height = $height/2.6;
                        }elseif ($width>700) {
                            $thumb_width = $width/2.3;
                            $thumb_height = $height/2.3;
                        }elseif ($width>600) {
                            $thumb_width = $width/2;
                            $thumb_height = $height/2;
                        }elseif ($width>500) {
                            $thumb_width = $width/1.9;
                            $thumb_height = $height/1.9;
                        }elseif ($width>400) {
                            $thumb_width = $width/1.3;
                            $thumb_height = $height/1.3;
                        }elseif ($width>300) {
                            $thumb_width = $width/1.2;
                            $thumb_height = $height/1.2;
                        }else{
                            $thumb_width = $width;
                            $thumb_height = $height;
                        }
                        $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                        switch($file_ext){
                            case 'jpg':
                                $source = imagecreatefromjpeg($upload_image);
                                break;
                            case 'jpeg':
                                $source = imagecreatefromjpeg($upload_image);
                                break;

                            case 'png':
                                $source = imagecreatefrompng($upload_image);
                                break;
                            case 'gif':
                                $source = imagecreatefromgif($upload_image);
                                break;
                            default:
                                $source = imagecreatefromjpeg($upload_image);
                        }

                        imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                        switch($file_ext){
                            case 'jpg' || 'jpeg':
                                imagejpeg($thumb_create,$thumbnail,100);
                                break;
                            case 'png':
                                imagepng($thumb_create,$thumbnail,100);
                                break;

                            case 'gif':
                                imagegif($thumb_create,$thumbnail,100);
                                break;
                            default:
                                imagejpeg($thumb_create,$thumbnail,100);
                        }

                    

                    $return = Array('ok'=>TRUE,'logo'=>'');
                }
                else{
                    $return = Array('ok' => FALSE, 'logo' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                }
            //===========================================================================
        }
        $id=$data['sucursalid'];
        unset($data['sucursalid']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('sucursales',$data,'sucursalid',$id);
        }else{
            $this->ModeloCatalogos->Insert('sucursales',$data);
        }
    }
    public function getData_listado(){
        $params = $this->input->post();
        $productos = $this->ModeloCatalogos->List_table_sucursal($params);
        $totalRecords=$this->ModeloCatalogos->filastotal_sucursal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
}