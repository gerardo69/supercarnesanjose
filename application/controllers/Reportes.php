<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->library('Pdf');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloVentas');
    }
	public function index(){
        redirect('/Sistema');   
	}
    function ticket_venta($idventa,$monto=0){
        //============================
            $whereticket= array('id_ticket'=>1);
            $datosticket=$this->ModeloCatalogos->getselectvalue1rowwheren('ticket',$whereticket);
            foreach ($datosticket->result() as $item) {

                $tck_titulo=$item->titulo;
                $tck_mensajea=$item->mensajea;
                $tck_mensajeb=$item->mensajeb;
                $tck_fuente=$item->fuente;
                $tck_margen_superior=$item->margen_superior;
                $tck_tamanio=$item->tamanio;
                $barcodeid=$item->barcode;
                $anchot=$item->anchot;
            }
            $namecodigo=$this->ModeloCatalogos->codebar($barcodeid);
        
        //=========================
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array($anchot, 250), true, 'UTF-8', false);

            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Gerry');
            $pdf->SetTitle('Recibo');
            $pdf->SetSubject('Recibo de pago de viaje');
            $pdf->SetKeywords('Recibo, Pago, Comprobante');
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // set margins
            $pdf->SetMargins('8',$tck_margen_superior,'8');
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            $pdf->SetFont($tck_fuente, '', $tck_tamanio);
        //=========================

        // add a page
            $pdf->AddPage();
            $sucursalId=0;
            $resultv=$this->ModeloVentas->getventa($idventa);
            foreach ($resultv->result() as $item) {
                $ventaId = $item->ventaId;
                $vendedor = $item->vendedor;
                $personalId = $item->personalId;
                $cliente = $item->cliente;
                $subtotal = $item->subtotal;
                $ndescuento = $item->ndescuento;
                $descuento = $item->descuento;
                $total = $item->total;
                $tipopago = $item->tipopago;
                $fechavencimiento = $item->fechavencimiento;
                $pagado = $item->pagado;
                $metodo = $item->metodo;
                $cancelado = $item->cancelado;
                $canceladoh = $item->canceladoh;
                $reg = $item->reg;
                $fechav = date("d-m-Y",strtotime($reg));
                $horav = date("G:i",strtotime($reg));
                $sucursalId=$item->sucursalid;

            }
        //=====================================================================
            if ($barcodeid<=18) {
                    $tipocodigobar='write1DBarcode';
                    $stylebc= array('position'=>'C', 
                                    'border'=>false, 
                                    'padding'=>4, 
                                    'fgcolor'=>array(0,0,0), 
                                    'bgcolor'=>array(255,255,255), 
                                    'text'=>true, 
                                    'font'=>'helvetica', 
                                    'fontsize'=>8, 
                                    'stretchtext'=>4);
                    $params = $pdf->serializeTCPDFtagParameters(array($ventaId, $namecodigo, '', '', 30, 30, 0.4, $stylebc, 'N'));
                }else{
                    $tipocodigobar='write2DBarcode';
                    $styleQR = array('border' => 0, 
                         'vpadding' => '0', 
                         'hpadding' => '0', 
                         'fgcolor' => array(0, 0, 0), 
                         'position' => 'C',
                         'bgcolor' => false, 
                         'module_width' => 1, 
                         'module_height' => 1);
                    $params = $pdf->serializeTCPDFtagParameters(array($ventaId, $namecodigo, '', '', 30, 30, $styleQR, 'N'));
                }
        //====================================================================================================
            $sucrusalv=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','sucursalid',$sucursalId);
            $sucursalname='';
            $sucursallogo='';
             $sucursaldireccion='';
            foreach ($sucrusalv->result() as $item) {
                $sucursalname=$item->sucursal;
                $sucursallogo=$item->logo;
                $sucursaldireccion=$item->direccion;
            }
            if ($sucursallogo=='') {
                $imglogo=base_url().'public/images/pos.svg';
            }else{
                $imglogo=base_url().'public/images/sucursal/'.$sucursallogo;
            }


        $html = '';
        $html .= '<table border="0">
                    
                    <tr>
                        <td align="center">'.$tck_titulo.'</td>
                    </tr>
                    
                    ';
            if ($sucursalname!='') {
                $html .= '<tr>
                        <td align="center">'.$sucursalname.'</td>
                    </tr>';
            }
            if ($sucursaldireccion!='') {
                $html .= '<tr>
                        <td align="center">'.$sucursaldireccion.'</td>
                    </tr>';
            }
           $html .= '<tr>
                        <td align="center">'.$tck_mensajea.'</td>
                    </tr>';
          $html .= '<tr>
                        <td align="center"></td>
                    </tr>
                 </table>';

        $html .= '<table border="0">';
            $html .= '<tr>
                        <td>Fecha: '.$fechav.'</td>
                      </tr>
                      <tr>
                        <td>Hora: '.$horav.'</td>
                      </tr>
                      <tr>
                        <td>No ticket: '.$ventaId.'</td>
                      </tr>
                      <tr>
                        <td></td>
                      </tr>
                    ';
            if ($tipopago==2) {
                $html .= '
                      <tr>
                        <td>Compra a credito</td>
                      </tr>
                    ';
            }
        $html .= '</table>';
        $html .= '<table border="0">';
        $html .= '<tr>
                        <td width="20%" style="background-color:#5b5c5d;color:white;"><b>Cant</b>.</td>
                        <td width="40%" style="background-color:#5b5c5d;color:white;"><b>Articulo</b></td>
                        <td width="20%" style="background-color:#5b5c5d;color:white;"><b>Precio</b></td>
                        <td width="20%" style="background-color:#5b5c5d;color:white;"><b>Total</b></td>
                      </tr>';
        $resultvd=$this->ModeloVentas->getventad($idventa);
        foreach ($resultvd->result() as $item) {
            $html .= '<tr>
                        <td align="center">'.$item->cantidad.'</td>
                        <td>'.$item->nombre.'</td>
                        <td align="center">'.$item->precio.'</td>
                        <td align="center">'.$item->precio*$item->cantidad.'</td>
                      </tr>';
        }
            $html .= '<tr>
                        <td colspan="4"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="2">SubTotal</td>
                        <td align="center">'.$subtotal.'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="2">Descuento</td>
                        <td align="center">'.$descuento.'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="2"><b>Total</b></td>
                        <td align="center">'.$total.'</td>
                      </tr>';
            if($monto>0){
                $html .= '<tr>
                            <td></td>
                            <td colspan="2"><b>Cambio</b></td>
                            <td align="center">'.$monto.'</td>
                          </tr>';
            }

        $html .= '</table>';




        $html .= '<table border="0">
                    
                    <tr>
                        <td align="center"></td>
                    </tr>
                    <tr>
                        <td align="center">'.$tck_mensajeb.'</td>
                    </tr>
                 </table>';


        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        $pdf->Output('Venta.pdf', 'I');

    }
    function ticket_venta2($idventa,$monto=0){
        //============================
            $whereticket= array('id_ticket'=>1);
            $datosticket=$this->ModeloCatalogos->getselectvalue1rowwheren('ticket',$whereticket);
            foreach ($datosticket->result() as $item) {

                $tck_titulo=$item->titulo;
                $tck_mensajea=$item->mensajea;
                $tck_mensajeb=$item->mensajeb;
                $tck_fuente=$item->fuente;
                $tck_margen_superior=$item->margen_superior;
                $tck_tamanio=$item->tamanio;
                $barcodeid=$item->barcode;
                $anchot=$item->anchot;
            }
            $namecodigo=$this->ModeloCatalogos->codebar($barcodeid);

        // add a page
            $sucursalId=0;
            $resultv=$this->ModeloVentas->getventa($idventa);
            foreach ($resultv->result() as $item) {
                $ventaId = $item->ventaId;
                $vendedor = $item->vendedor;
                $personalId = $item->personalId;
                $cliente = $item->cliente;
                $subtotal = $item->subtotal;
                $ndescuento = $item->ndescuento;
                $descuento = $item->descuento;
                $total = $item->total;
                $tipopago = $item->tipopago;
                $fechavencimiento = $item->fechavencimiento;
                $pagado = $item->pagado;
                $metodo = $item->metodo;
                $cancelado = $item->cancelado;
                $canceladoh = $item->canceladoh;
                $reg = $item->reg;
                $fechav = date("d-m-Y",strtotime($reg));
                $horav = date("G:i",strtotime($reg));
                $sucursalId=$item->sucursalid;

            }

            $sucrusalv=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','sucursalid',$sucursalId);
            $sucursalname='';
            $sucursallogo='';
             $sucursaldireccion='';
            foreach ($sucrusalv->result() as $item) {
                $sucursalname=$item->sucursal;
                $sucursallogo=$item->logo;
                $sucursaldireccion=$item->direccion;
            }
            


        $html = '<style type="text/css">
                  @media print{
                    .trtdticket{
                      background-color:#5b5c5d;color:white;
                    }
                  }
                </style>';
        $html .= '<table border="0" class="tableticket">
                    
                    <tr>
                        <td align="center">'.$tck_titulo.'</td>
                    </tr>
                    
                    ';
            if ($sucursalname!='') {
                $html .= '<tr>
                        <td align="center">'.$sucursalname.'</td>
                    </tr>';
            }
            if ($sucursaldireccion!='') {
                $html .= '<tr>
                        <td align="center">'.$sucursaldireccion.'</td>
                    </tr>';
            }
           $html .= '<tr>
                        <td align="center">'.$tck_mensajea.'</td>
                    </tr>';
          $html .= '<tr>
                        <td align="center"></td>
                    </tr>
                 </table>';

        $html .= '<table border="0" class="tableticket">';
            $html .= '<tr>
                        <td>Fecha: '.$fechav.'</td>
                      </tr>
                      <tr>
                        <td>Hora: '.$horav.'</td>
                      </tr>
                      <tr>
                        <td>No ticket: '.$ventaId.'</td>
                      </tr>
                      <tr>
                        <td></td>
                      </tr>
                    ';
            if ($tipopago==2) {
                $html .= '
                      <tr>
                        <td>Compra a credito</td>
                      </tr>
                    ';
            }
        $html .= '</table>';
        $html .= '<table border="0" class="tableticket">';
        $html .= '<tr>
                        <td width="20%" class="trtdticket" style="background-color:#5b5c5d;color:white;"><b>Cant</b>.</td>
                        <td width="40%" class="trtdticket" style="background-color:#5b5c5d;color:white;"><b>Articulo</b></td>
                        <td width="20%" class="trtdticket" style="background-color:#5b5c5d;color:white;"><b>Precio</b></td>
                        <td width="20%" class="trtdticket" style="background-color:#5b5c5d;color:white;"><b>Total</b></td>
                      </tr>';
        $resultvd=$this->ModeloVentas->getventad($idventa);
        foreach ($resultvd->result() as $item) {
            $html .= '<tr>
                        <td >'.$item->cantidad.'</td>
                        <td>'.$item->nombre.'</td>
                        <td >'.$item->precio.'</td>
                        <td >'.$item->precio*$item->cantidad.'</td>
                      </tr>';
        }
            $html .= '<tr>
                        <td colspan="4"><br></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="2">SubTotal</td>
                        <td >'.$subtotal.'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="2">Descuento</td>
                        <td >'.$descuento.'</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="2"><b>Total</b></td>
                        <td >'.$total.'</td>
                      </tr>';
            if($monto>0){
                $html .= '<tr>
                            <td></td>
                            <td colspan="2"><b>Cambio</b></td>
                            <td align="center">'.$monto.'</td>
                          </tr>';
            }

        $html .= '</table>';




        $html .= '<table border="0" class="tableticket">
                    
                    
                    <tr>
                        <td align="center"></td>
                    </tr>
                    <tr>
                        <td align="center">'.$tck_mensajeb.'</td>
                    </tr>
                 </table>';

        echo $html;         
        

    }
    function ticket_compras($idcompra){
        //============================
            $whereticket= array('id_ticket'=>1);
            $datosticket=$this->ModeloCatalogos->getselectvalue1rowwheren('ticket',$whereticket);
            foreach ($datosticket->result() as $item) {

                $tck_titulo=$item->titulo;
                $tck_mensajea=$item->mensajea;
                $tck_mensajeb=$item->mensajeb;
                $tck_fuente=$item->fuente;
                $tck_margen_superior=$item->margen_superior;
                $tck_tamanio=$item->tamanio;
                $barcodeid=$item->barcode;
                $anchot=$item->anchot;
            }
            $namecodigo=$this->ModeloCatalogos->codebar($barcodeid);
        
        //=========================
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array($anchot, 250), true, 'UTF-8', false);

            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Gerry');
            $pdf->SetTitle('Recibo');
            $pdf->SetSubject('Recibo de pago de viaje');
            $pdf->SetKeywords('Recibo, Pago, Comprobante');
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // set margins
            $pdf->SetMargins('8',$tck_margen_superior,'8');
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            $pdf->SetFont($tck_fuente, '', $tck_tamanio);
        //=========================

        // add a page
            $pdf->AddPage();
            $sucursalId=0;
            $resultv=$this->ModeloCatalogos->getdatoscompras($idcompra);
            foreach ($resultv->result() as $item) {
                $compraId = $item->compraId;
                $razon_social=$item->razon_social;
                $sucursalId=$item->sucursalid;
                $monto_total=$item->monto_total;
                $reg = $item->reg;
                $fechav = date("d-m-Y",strtotime($reg));
                $horav = date("G:i",strtotime($reg));
                

            }
        //=====================================================================
            if ($barcodeid<=18) {
                    $tipocodigobar='write1DBarcode';
                    $stylebc= array('position'=>'C', 
                                    'border'=>false, 
                                    'padding'=>4, 
                                    'fgcolor'=>array(0,0,0), 
                                    'bgcolor'=>array(255,255,255), 
                                    'text'=>true, 
                                    'font'=>'helvetica', 
                                    'fontsize'=>8, 
                                    'stretchtext'=>4);
                    $params = $pdf->serializeTCPDFtagParameters(array($compraId, $namecodigo, '', '', 30, 30, 0.4, $stylebc, 'N'));
                }else{
                    $tipocodigobar='write2DBarcode';
                    $styleQR = array('border' => 0, 
                         'vpadding' => '0', 
                         'hpadding' => '0', 
                         'fgcolor' => array(0, 0, 0), 
                         'position' => 'C',
                         'bgcolor' => false, 
                         'module_width' => 1, 
                         'module_height' => 1);
                    $params = $pdf->serializeTCPDFtagParameters(array($compraId, $namecodigo, '', '', 30, 30, $styleQR, 'N'));
                }
        //====================================================================================================
            $sucrusalv=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','sucursalid',$sucursalId);
            $sucursalname='';
            $sucursallogo='';
             $sucursaldireccion='';
            foreach ($sucrusalv->result() as $item) {
                $sucursalname=$item->sucursal;
                $sucursallogo=$item->logo;
                $sucursaldireccion=$item->direccion;
            }
            if ($sucursallogo=='') {
                $imglogo=base_url().'public/images/pos.svg';
            }else{
                $imglogo=base_url().'public/images/sucursal/'.$sucursallogo;
            }


        $html = '';
        $html .= '<table border="0">
                    <tr>
                        <td align="center">
                            <img src="'.$imglogo.'" height="30">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">'.$tck_titulo.'</td>
                    </tr>
                    
                    ';
            if ($sucursalname!='') {
                $html .= '<tr>
                        <td align="center">'.$sucursalname.'</td>
                    </tr>';
            }
            if ($sucursaldireccion!='') {
                $html .= '<tr>
                        <td align="center">'.$sucursaldireccion.'</td>
                    </tr>';
            }
           $html .= '<tr>
                        <td align="center">'.$tck_mensajea.'</td>
                    </tr>';
          $html .= '<tr>
                        <td align="center"></td>
                    </tr>
                 </table>';

        $html .= '<table border="0">';
            $html .= '<tr>
                        <td>Fecha: '.$fechav.'</td>
                      </tr>
                      <tr>
                        <td>Hora: '.$horav.'</td>
                      </tr>
                      <tr>
                        <td>No ticket compra: '.$compraId.'</td>
                      </tr>
                      <tr>
                        <td></td>
                      </tr>
                    ';
           
        $html .= '</table>';
        $html .= '<table border="0">';
        $html .= '<tr>
                        <td width="20%" style="background-color:#5b5c5d;color:white;"><b>Cant</b>.</td>
                        <td width="40%" style="background-color:#5b5c5d;color:white;"><b>Articulo</b></td>
                        <td width="20%" style="background-color:#5b5c5d;color:white;"><b>Precio</b></td>
                        <td width="20%" style="background-color:#5b5c5d;color:white;"><b>Total</b></td>
                      </tr>';
        $resultvd=$this->ModeloCatalogos->getdatoscomprasd($idcompra);
        foreach ($resultvd->result() as $item) {
            $html .= '<tr>
                        <td align="center">'.$item->cantidad.'</td>
                        <td>'.$item->nombre.'</td>
                        <td align="center">'.$item->precio_compra.'</td>
                        <td align="center">'.$item->precio_compra*$item->cantidad.'</td>
                      </tr>';
        }
            $html .= '<tr>
                        <td colspan="4"></td>
                      </tr>
                      
                      <tr>
                        <td></td>
                        <td colspan="2"><b>Total</b></td>
                        <td align="center">'.$monto_total.'</td>
                      </tr>';
            

        $html .= '</table>';




        $html .= '<table border="0">
                    <tr>
                        <td align="center"></td>
                    </tr>
                    <tr>
                        <td align="center"><tcpdf method="'.$tipocodigobar.'" params="' . $params . '" /></td>
                    </tr>
                    <tr>
                        <td align="center"></td>
                    </tr>
                    <tr>
                        <td align="center">'.$tck_mensajeb.'</td>
                    </tr>
                 </table>';


        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        $pdf->Output('Venta.pdf', 'I');

    }
}