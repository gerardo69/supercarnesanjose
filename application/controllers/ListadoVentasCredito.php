<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListadoVentasCredito extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');

        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->personal=$this->session->userdata('idpersonal');
            $this->sucursalId=$this->session->userdata('sucursalId');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,18);// 15 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
	public function index(){ 
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('listadoventas/credito');
        $this->load->view('templates/footer');
        $this->load->view('listadoventas/jscredito');
	}
    public function add(){
        $ventaId = $this->input->post('ventaId');
        $pago = $this->input->post('pago');
        $pago = number_format($pago,2); 
        log_message('error', 'venta pago a:'.$pago);
        // Octenemos el total del producto comprado 

        $total_apagar =  $this->ModeloCatalogos->verificarcantidad($ventaId);
          
        // Octenemos los pagos que an ingresado
        $resulpago = $this->ModeloCatalogos->verificarpago($ventaId);
        $pagos_registados=0;
        foreach ($resulpago->result() as $key) {
               $pagos_registados = $key->suma;
        }
        log_message('error', 'total_apagar:'.$total_apagar);
        log_message('error', 'pagos_registados:'.$pagos_registados);

        // A qui se hace una resta del total con los pagos
        $restapagar = $total_apagar-$pagos_registados;
        $restapagar=number_format($restapagar,2);
        log_message('error', 'restapagar:'.$restapagar);
        log_message('error', '');
        log_message('error', $pago.'<='.$restapagar);
        log_message('error', gettype($pago).'<='.gettype($restapagar));
        
    
        if($pago<=$restapagar){
            $x=0;
            $data['ventaId'] = $ventaId;
            $data['pago'] = $pago;
            $data['personalId'] = $this->personal;
            $data['reg'] = $this->fechahoy;
            log_message('error', 'venta pago b:'.$pago);
            $this->ModeloCatalogos->Insert('pagos_credito',$data);
            log_message('error', 'Pago registrado');
            $comprobar = $restapagar-$pago;
            if($comprobar<=0){
               $pagoid = array('pagado' =>1 , ); 
               log_message('error', 'venta pagada id:'.$ventaId);
               $this->ModeloCatalogos->updateCatalogo('ventas',$pagoid,'ventaId',$ventaId);
            }
        }else{
            $x=1; 
            log_message('error', 'Pago no registrado');
        }
    

    echo $x;
    }

    public function pagos(){
        $num = $this->input->post('ventaId');
        $resulpago = $this->ModeloCatalogos->verificarpago($num);
        foreach ($resulpago->result() as $key) {
               $suma = $key->suma;
               $total = $key->total;
        }
        $resta = $total-$suma;
        $result = $this->ModeloCatalogos->List_table_pagos($num);
        $html = '';
            $html .= '<table class="table table-striped jambo_table bulk_action" id="data-tables">';
            $html .= '<thead>';
              $html .= '<tr>';
                $html .= '<th>#</th>';
                $html .= '<th>Pago</th>';
                $html .= '<th>Fecha deposito</th>';
              $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
              foreach ($result->result() as $item) {
                    $html .="<tr>";
                      $html .= "<td>".$item->pagoId."</td>";
                      $html .= "<td>".$item->pago."</td>";
                      $html .= "<td>".$item->reg."</td>";
                    $html .= "</tr>";
                 }
            $html .= "</tbody>";
          $html .= "</table>";
          $html .= "<div class='row'>";
              $html .= "<div class='col-md-4 col-sm-4 col-xs-12'>Total: <b>$".bcdiv($suma, '1', 1)."</b></div><div class='col-md-4 col-sm-4 col-xs-12'>Resta: <b>$".bcdiv($resta, '1', 1)."</b> </div>";
            $html .= "</div>";
            $html .= "<br>";
        $html .= '';
        
        echo $html;

    }

    function cancelar(){
        $ventaId = $this->input->post('ventaId');
        $cancela_motivo = $this->input->post('cancela_motivo');
        $sumastock = $this->ModeloCatalogos->getventasdestalle($ventaId);
        foreach ($sumastock->result() as $item) {
            $cantidad = $item->cantidad;
            $idproducto = $item->productoid;
            $productos = $this->ModeloCatalogos->getproducto($idproducto);
            foreach ($productos->result() as $item) {
                $stock1 = $item->stock1; 
            }
            $suma = $cantidad + $stock1;
            $arraystock1 = array('stock1' => $suma);
            $result = $this->ModeloCatalogos->updateCatalogo('productos',$arraystock1,'productoid',$idproducto);
        }
        $data = array('cancelado'=>1,'cancela_personal'=> $this->personal,'cancela_motivo' => $cancela_motivo,'canceladoh'=> $this->fechahoy );
        $result = $this->ModeloCatalogos->updateCatalogo('ventas',$data,'ventaId',$ventaId);
    }
    public function getData_listado(){
        $params = $this->input->post();
        $productos = $this->ModeloCatalogos->List_table_ventascredito($params,$this->sucursalId);
        $totalRecords=$this->ModeloCatalogos->filastotal_ventascredito($params,$this->sucursalId); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
}