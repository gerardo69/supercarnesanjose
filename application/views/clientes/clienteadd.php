<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Cliente <small><?php echo $subtitle;?></small></h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Datos generales <small></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form class="form-horizontal form-label-left input_mask" method="post"  role="form" id="formclientes">
                <input type="hidden" class="form-control" id="ClientesId" name="ClientesId" value="<?php echo $ClientesId ;?>">
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Nombre</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" id="Nombre" name="Nombre" value="<?php echo $Nombre;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Domicilio</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" id="Domicilio" name="Domicilio" value="<?php echo $Domicilio;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Ciudad</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="Ciudad" name="Ciudad" value="<?php echo $Ciudad;?>"> 
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Estado/Provincia/Region</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="Estado" name="Estado" value="<?php echo $Estado;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Codigo Postal</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="CodigoPostal" name="CodigoPostal" value="<?php echo $CodigoPostal;?>">
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Correo</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="email" class="form-control" id="Correo" name="Correo" value="<?php echo $Correo;?>">
                  </div>
                </div>
                <div class="x_title">
                  <h2>Datos de contacto <small></small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Nombre del contacto</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" id="nombrec" name="nombrec" value="<?php echo $nombrec;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">E-mail</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="email" class="form-control" id="correoc" name="correoc" value="<?php echo $correoc;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Telefono</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="telefonoc" name="telefonoc" value="<?php echo $telefonoc;?>">
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Extencion</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="extencionc" name="extencionc" value="<?php echo $extencionc;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">celular</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="celular" name="celular" value="<?php echo $celular;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Descripcion</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <textarea class="form-control" id="descripcionc" name="descripcionc"><?php echo $descripcionc ;?></textarea>
                  </div>

                </div>
                <br>
              </form>
              <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>Clientes"><i class="fa fa-reply"></i> Regresar</a>
                    <button type="submit" class="btn btn-success guardar"><?php echo $button;?></button>
                  </div>
                </div>
            </div>
          </div>

        </div>
    </div>

  </div>
</div>    