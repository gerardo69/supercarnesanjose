<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Clientes <small>Listado</small></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-9 col-xs-12">
          <!--------//////////////-------->
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
              <div class="row text-right">
                <a class="btn btn-dark float-right" href="<?php echo base_url(); ?>Clientes/Clienteadd">Nuevo</a>
              </div>     
            </div>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data_tables">
            <thead>
              <tr >
                <th>#</th>
                <th>Nombre</th>
                <th>Domicilio</th>
                <th>Correo</th>
                <th>Contacto</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>
<!-- /page content -->

<!-- /page content -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="eliminar_modal">
  <div class="modal-dialog">
    <div class="modal-content curba">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Confirmación</h3>
      </div>
      <div class="modal-body">
        <h4>¿Está seguro que desea eliminar a <b class="nom"></b>?</h4>
        <input type="hidden" name="ClientesId" id="ClientesId">
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="boton_eliminar()"><i class="fa fa-trash"></i> Eliminar</button>
      </div>

    </div>
  </div>
</div>  