<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Ventas Normales <small>Listado</small></h3> 
          </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <!--------//////////////-------->
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
              <div class="row text-right">
     
              </div>
            </div>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data_tables">
            <thead>
              <tr>
                <th>#</th>
                <th>Cliente</th>
                <th>Subtotal</th>
                <th>% Descuento</th>
                <th>Descuento</th>
                <th>Total</th>
                <th>Fecha</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>

          </table>

          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="cancelar_modal">
  <div class="modal-dialog ">
    <div class="modal-content curba">
      <div class="modal-header curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Confirmación</h3>
      </div>
      <div class="modal-body">
        <h4>¿Está seguro que desea cancelar esta venta?</h4>
         <form class="form-horizontal form-label-left input_mask">
            <div class="form-group">
                      <label class="label-text col-md-2 col-sm-2 col-xs-12">Motivo</label>
                      <div class="col-md-10 col-sm-10 col-xs-12">
                         <textarea class="form-control" rows="3" id="cancela_motivo" name="cancela_motivo"></textarea>
                      </div>
            </div>  
            <input type="hidden" name="ventaId_venta" id="ventaId_venta">
          </form>
        <br>
      </div>
      <div class="modal-footer curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="btn_cancelar()">Cancelar</button>
      </div>
    </div>
  </div>
</div>  
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="cancelado_modal">
  <div class="modal-dialog ">
    <div class="modal-content curba">

      <div class="modal-header curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Datos de la Cancelación</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left input_mask">
            <div class="form-group">
                      <label class="label-text col-md-2 col-sm-2 col-xs-12">Persona</label>
                      <div class="col-md-10 col-sm-10 col-xs-12">
                         <output type="text" id="cper" style="border: 0px">
                      </div>
            </div>     
            <div class="form-group">
                      <label class="label-text col-md-2 col-sm-2 col-xs-12">Motivo</label>
                      <div class="col-md-10 col-sm-10 col-xs-12">
                         <output type="text" id="mot" style="border: 0px">
                      </div>
            </div>     
            <div class="form-group">
                      <label class="label-text col-md-2 col-sm-2 col-xs-12">Fecha</label>
                      <div class="col-md-10 col-sm-10 col-xs-12">
                         <output type="text" id="fec" style="border: 0px">
                      </div>
            </div> 
          </form>
      </div>
      <div class="modal-footer curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div> 