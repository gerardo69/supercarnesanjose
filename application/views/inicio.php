<style type="text/css">
  .imgpro{
    height: 50px;
  }
</style>
<!-- page content -->
        <div class="right_col" role="main">
        
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Reporte <small>Ventas del <?php echo $anioactual ?></small></h3>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Compras y Ventas <small><?php echo $anioactual ?></small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          <canvas id="graficaventas"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="bs-glyphicons">
                          <ul class="bs-glyphicons-list">
                            <li style="height: 95px;width: 400px;">
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                <img style="height: 60px;width: 60px;" src="<?php echo base_url();?>public/images/inve.png">
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Invetario Neto</h5>
                                <h4><?php echo $productostock;?></h4>
                                <h6>Productos en stock: <?php echo $productostotal;?></h6>
                                </div>
                            </li>
                          </ul>
                          <ul class="bs-glyphicons-list">
                            <li style="height: 95px;width: 400px;">
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                <img style="height: 60px;width: 60px;" src="<?php echo base_url();?>public/images/bille.png">
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Ventas <?php echo date('Y');?></h5>
                                <h4><?php echo number_format($ventastotal,2,'.',',');?></h4>
                                </div>
                            </li>
                          </ul>
                          <ul class="bs-glyphicons-list">
                             <li style="height: 95px;width: 400px;">
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                <img style="height: 60px;width: 60px;" src="<?php echo base_url();?>public/images/carri.png">
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Compras <?php echo date('Y');?></h5>
                                <h4><?php echo number_format($comprasy['total'],2,'.',',');?></h4>
                                <h6>Compras realizadas: <?php echo number_format($comprasy['reg'],0,'.',',');?></h6>
                                </div>
                            </li>
                          </ul>
                          <ul class="bs-glyphicons-list">
                            <li style="height: 95px;width: 400px;">
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                <img style="height: 60px;width: 60px;" src="<?php echo base_url();?>public/images/clien.png">
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                <h5>Clientes</h5>
                                <h4><?php echo $clientestotal;?></h4>
                                </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Ultimas <small>Ventas</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>Factura Nº</th>
                              <th>Cliente</th>
                              <th>Fecha</th>
                              <th>Total</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($ventasnew->result() as $item){ ?>
                                <tr>
                                  <th scope="row"><?php echo $item->ventaId ;?> </th>
                                  <td><?php echo $item->Nombre ;?></td>
                                  <td><?php echo $item->reg ;?></td>
                                  <td>$<?php echo $item->total ;?></td>
                                </tr>
                             <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Nuevos productos <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
  
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Foto</th>
                          <th>producto</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($productosnew->result() as $item){ ?>
                          <tr>
                            <td>
                              <?php 
                                  if ($item->img!='') {
                                    $img=base_url().'public/images/productost/'.$item->img;
                                  }else{
                                    $img=base_url().'public/images/pos.svg';
                                  }
                              ?>
                            <img src="<?php echo $img; ?>" class="imgpro">
                            </td>
                            <td><?php echo $item->nombre; ?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                </div>
              </div>
            </div>

          </div>
          <br />
        </div>
