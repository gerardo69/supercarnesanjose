<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="authoring-tool" content="BeaverDS">
    <meta name="description" content="Punto de venta , BeaverDS">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript,punto de venta,BeaverDS">
    <meta name="author" content="Beaver ds">
    <meta name="og:description" content="Punto de venta , BeaverDS."/>
    <meta name="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="og:title" content="Beaver DS"/>
    <meta name="og:image" content="https://beaverds.com/logo.png"/>

    <link rel="icon" href="<?php echo base_url(); ?>public/images/favicon.ico?v=2018082101" type="image/x-icon" />
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>public/images/favicon.png" type="image/png" />
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>public/images/favicon.png" type="image/png" />
    <link rel="apple-touch-icon" sizes="194x194" href="<?php echo base_url(); ?>public/images/favicon.png" type="image/png" />
    <meta name="theme-color" content="#2c98bb">


    <title>Pos | BeaverDS </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url(); ?>vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>build/css/custom.min.css" rel="stylesheet">
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
    <!-- PNotify -->
    <link href="<?php echo base_url(); ?>vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    <link href="<?php echo base_url(); ?>public/css/fileinput-rtl.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>public/css/fileinput.min.css" rel="stylesheet" type="text/css">
  </head>
  <style type="text/css">
    .label-text{padding-top: 8px;}
    .curbaa{border-top-right-radius: 10px;border-top-left-radius: 10px;}
    .curbab{border-bottom-right-radius: 10px;border-bottom-left-radius: 10px}
    .curba{border-radius: 10px;}
    .foot{height: 55px;}
    .vd_green{color: #00ff08;}
  </style>