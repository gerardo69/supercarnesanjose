<link rel="stylesheet" class="printcss" type="text/css" href="<?php echo base_url(); ?>public/css/printecorte.css" media="print">
<!-- page content -->
<div class="right_col" role="main">
  
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Reportes </h3>
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <!--------//////////////-------->
            <div class="row divprinter">
              <label class="col-md-1 col-sm-1 col-xs-12 control-label">Desde:</label>
              <div class="col-md-2 col-sm-3 col-xs-12">
                <input type="date" name="fechaini" id="fechaini" value="<?php echo date('Y-m-d');?>" class="form-control">
              </div>
              <label class="col-md-1 col-sm-1 col-xs-12 control-label">Hasta:</label>
              <div class="col-md-2 col-sm-3 col-xs-12">
                <input type="date" name="fechafin" id="fechafin" value="<?php echo date('Y-m-d');?>" class="form-control">
              </div>
              <label class="col-md-1 col-sm-1 col-xs-12 control-label">Informe:</label>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <select class="form-control" id="tipo">
                  <option value="1">Ventas contado</option>
                  <option value="2">Ventas crédito</option>
                  <option value="3">Compras</option>
                  <option value="4">Productos vendidos</option>
                  <option value="5">Productos bajos de inventario</option>
                </select>

              </div>
              <div class="col-md-2 col-sm-12 col-xs-12">
                <button class="btn btn-primary btnBuscar" type="button">Buscar</button>
                <button class="btn btn-primary imprimir" type="button"><i class="fa fa-print"></i></button>
              </div>

            </div>
            <div class="row addinforme">
              
            </div>
          

          
          
          <!--------//////////////-------->
          
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <br />
</div>
<!-- /page content -->