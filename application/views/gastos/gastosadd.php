<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Gastos <small></small></h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Datos generales <small></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form class="form-horizontal form-label-left input_mask" method="post"  role="form" id="formgastos">
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Fecha</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="date" class="form-control" id="fecha" name="fecha">
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Cantidad</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="number" step="any" class="form-control" id="cantidad" name="cantidad">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Concepto</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="concepto" name="concepto">
                  </div>
                </div>
                <div class="form-group" <?php if($perfilid>1){echo 'style="display: none;"';}?>  >
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Sucursal</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <select name="sucursal" class="form-control" id="sucursalid">
                        <?php 
                          foreach ($sucursal->result() as $row) { ?>
                            <option value="<?php echo $row->sucursalid;?>" <?php if ($sucursalIdd==$row->sucursalid) { echo 'selected';} ?> ><?php echo $row->sucursal?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Comentarios</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <textarea class="form-control" rows="3" id="comentarios" name="comentarios"></textarea>
                  </div>
                </div>
                <br>
              </form>
              <hr>
              <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>Gastos"><i class="fa fa-reply"></i> Regresar</a>
                    <button type="submit" class="btn btn-success guardargatos">Guardar</button>
                  </div>
                </div>
            </div>
          </div>

        </div>
    </div>

  </div>
</div>    