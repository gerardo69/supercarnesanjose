<style type="text/css">
  .dccliente{
    font-size: 9px;
  }
</style>
<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Compras <small>Listado</small></h3>
          </div>
        </div>
        <div class="col-md-12 col-sm-9 col-xs-12">
          <!--------//////////////--------> 
          <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
              <div class="row text-right">
                <a class="btn btn-dark float-right" href="<?php echo base_url(); ?>Compras/Comprasadd">Nuevo</a>
              </div>   
            </div>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data_tables">
            <thead>
              <tr >
                <th>#</th>
                <th>Proveedor</th>
                <th>Monto</th>
                <th>Fecha</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php
              /*
               foreach ($Compras->result() as $item){ ?>
                <tr class="num_<?php echo $item->compraId; ?>">
                  <td><?php echo $item->compraId; ?></td>
                  <td><?php echo $item->razon_social; ?></td>
                  <td><?php echo $item->monto_total; ?></td>
                  <td><?php echo $item->reg; ?></td>
                  

                  
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>
                      <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="#" onclick="productoscompras(<?php echo $item->compraId; ?>)" >Productos</a>
                          <a href="<?php echo base_url(); ?>Reportes/ticket_compras/<?php echo $item->compraId; ?>"  target="_blank">Ticket</a>
                        </li>
                       

                      </ul>
                    </div>
                  </td>
                </tr>
              <?php }
             */
               ?>
              
            </tbody>
          </table>
          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>
<!-- /page content -->

<!-- /page content -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="modal_productos">
  <div class="modal-dialog modal-lg">
    <div class="modal-content curba">

      <div class="modal-header  curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Productos de la compra</h3>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12  viewproductos" >
            
          </div> 
          
        </div>
        

      </div>
      <div class="modal-footer  curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>  