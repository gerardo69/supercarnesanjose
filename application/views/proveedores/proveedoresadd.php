<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Proveedor <small><?php echo $subtitle;?></small></h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Datos generales <small></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form class="form-horizontal form-label-left input_mask" method="post"  role="form" id="formproveedor">
                <input type="hidden" class="form-control" id="id_proveedor" name="id_proveedor" value="<?php echo $id_proveedor ;?>">
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Razon social</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" id="razon_social" name="razon_social" value="<?php echo $razon_social;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Domicilio</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" id="domicilio" name="domicilio" value="<?php echo $domicilio;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Ciudad</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="ciudad" name="ciudad" value="<?php echo $ciudad;?>">
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Estado/Provincia/Region</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="estado" name="estado" value="<?php echo $estado;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Codigo postal</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="cp" name="cp" value="<?php echo $cp;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Observaciones</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <textarea class="form-control" id="obser" name="obser"><?php echo $obser ;?></textarea>
                  </div>
                </div>
                <div class="x_title">
                  <h2>Datos de contacto <small></small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Nombre del contacto</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control has-feedback-left" id="contacto" name="contacto" value="<?php echo $contacto;?>">
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">E-mail</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="email" class="form-control has-feedback-left" id="email_contacto" name="email_contacto" value="<?php echo $email_contacto;?>">
                    <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">RFC</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="rfc" name="rfc" value="<?php echo $rfc;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Telefono local</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control has-feedback-left" id="telefono_local" name="telefono_local" value="<?php echo $telefono_local;?>">
                    <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Telefono celular</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control has-feedback-left" id="telefono_celular" name="telefono_celular" value="<?php echo $telefono_celular;?>">
                    <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
                <br>
              </form>
              <hr>
              <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>Proveedores"><i class="fa fa-reply"></i> Regresar</a>
                    <button type="submit" class="btn btn-success guardarprovee"><?php echo $button;?></button>
                  </div>
                </div>
            </div>
          </div>

        </div>
    </div>

  </div>
</div>    