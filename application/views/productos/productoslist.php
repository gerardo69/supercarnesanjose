<style type="text/css">
  .imgpro{
      height: 70px;
      border-radius: 5px;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

  }
  .imgpro:hover{
    transform: scale(1.5);
  }
</style>
<input type="hidden" id="stkper" readonly value="<?php echo $perfilid;?>">
<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Productos <small>Listado</small></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <!--------//////////////-------->
          
          <div class="row">
            <div class="col-md-8 text-left">
              
              <div class="form-group" <?php echo $list_block;?> >
                  <div class="col-md-7 col-sm-9 col-xs-12">
                     <select name="sucursalselected" class="form-control has-feedback-left" id="sucursalselected" onchange="recargartable();">
                        <?php foreach ($sucursalesrow->result() as $row) { ?>
                            <option value="<?php echo $row->sucursalid;?>" <?php if ($sucursalId==$row->sucursalid) { echo 'selected'; } ?>     ><?php echo $row->sucursal?></option>
                        <?php } ?>
                      </select>
                      <span class="fa fa-building-o form-control-feedback left" aria-hidden="true"></span>
                  </div>
              </div>
              
            </div>
            <div class="col-md-4">
              <div class="row text-right">
                <a class="btn btn-dark float-right" href="<?php echo base_url(); ?>Productos/Productosadd">Nuevo</a>
              </div>
              
              
              
            </div>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data-tables">
            <thead>
              <tr >
                <th>#</th>
                <th></th>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Categoria</th>
                <th>Stock</th>
                <th>Precio venta</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              

              
            </tbody>
          </table>
          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>
<!-- /page content -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="eliminar_modal">
  <div class="modal-dialog">
    <div class="modal-content curba">

      <div class="modal-header alert-danger curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Confirmación</h3>
      </div>
      <div class="modal-body">
        <h4>¿Está seguro de que desea eliminar el producto <b class="nom"></b>?</h4>
        <input type="hidden" name="productoid" id="productoid">
        <br>
      </div>

      <div class="modal-footer alert-danger curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-info" data-dismiss="modal" onclick="boton_eliminar()">Aceptar</button>
      </div>

    </div>
  </div>
</div>    