<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Producto <small><?php echo $subtitle;?></small></h3>
        </div>
    </div>
    <div class="clearfix"></div>
          <!--------//////////////-------->
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Datos generales <small></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
            <form class="form-horizontal form-label-left input_mask" method="post"  role="form" id="formproducto">
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Codigo:</label>
                  <div class="col-md-7 col-sm-9 col-xs-12">
                     <input type="text" class="form-control has-feedback-left" name="codigo" id="codigo" value="<?php echo $codigo;?>">
                     <span class="fa fa-barcode form-control-feedback left" aria-hidden="true"></span>
                     <input type="hidden" placeholder="Username" id="productoid" value="<?php echo $productoid;?>" name="productoid" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Nombre:</label>
                  <div class="col-md-7 col-sm-9 col-xs-12">
                     <input type="text" class="form-control has-feedback-left" name="nombre" id="nombre" value="<?php echo $nombre;?>">
                     <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Descripcion:</label>
                  <div class="col-md-7 col-sm-9 col-xs-12">
                     <textarea name="descripcion" class="form-control" id="descripcion"><?php echo $descripcion;?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Categoria:</label>
                  <div class="col-md-7 col-sm-9 col-xs-12">
                     <select name="categoria" class="form-control has-feedback-left" id="categoria">
                        <?php foreach ($categorias->result() as $row) { ?>
                            <option value="<?php echo $row->categoriaId;?>" <?php if ($categoria==$row->categoriaId) { echo 'selected'; } ?>     ><?php echo $row->categoria?></option>
                        <?php } ?>
                      </select>
                      <span class="fa fa-dropbox form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Precio Compra:</label>
                  <div class="col-md-7 col-sm-9 col-xs-12">
                     <input type="number" name="preciocompra" class="form-control has-feedback-left" id="preciocompra" value="<?php echo $preciocompra;?>">
                      <span class="fa fa-slack form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <input type="file" class="form-control cargaarchivo exampleInputFile" name="excel" id="exampleInputFile">
              </div>  
            </form>
           <div class="row">
             <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="col-md-12 col-sm-12 col-xs-12 table table-striped projects" id="tableproductossucursal">
                  <tbody>
                    <?php foreach ($sucursalesrow->result() as $itemp) { 
                      $datapro = $this->ModeloProductos->producto_existencia($id,$itemp->sucursalid); 
                      if ($perfilid==1) {
                        $permiso=1;
                      }else{
                        $permiso=0;
                      }
                      if ($permiso==0) {
                        if($sucursalId==$itemp->sucursalid){
                          $permiso=1;
                        }else{
                          $permiso=0;
                        }
                      }
                      if ($permiso==1) {
                      ?>
                    <tr>
                      <td>
                        <input type="hidden" id="idsucursal" value="<?php echo $itemp->sucursalid;?>" readonly>
                        <input type="hidden" id="idproductosucursal" value="<?php echo $datapro['idproductosucursal'];?>" readonly>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <h3><?php echo $itemp->sucursal;?></h3>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <label class="label-text">Precio Venta:</label>
                          <input type="number" class="form-control" name="precioventa" id="precioventa" value="<?php echo $datapro['precio_venta'];?>">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <label class="label-text">Precio mayoreo:</label>
                          <input type="number" class="form-control" name="mayoreo" id="mayoreo" value="<?php echo $datapro['mayoreo'];?>">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <label class="label-text">Cantidad mayoreo:</label>
                          <input type="number" class="form-control" name="can_mayoreo" id="can_mayoreo" title="Cantidad apartir de la cual se cobra a precio de mayoreo" value="<?php echo $datapro['can_mayoreo'];?>">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <label class="label-text">Existencia:</label>
                          <input type="number" class="form-control" name="existencia" id="existencia" value="<?php echo $datapro['existencia'];?>">
                        </div>
                      </td>
                    </tr>
                  <?php } } ?>
                  </tbody>
                </table>
             </div>
           </div> 
            <div class="col-md-12 ">
              <a class="btn btn-primary col-md-offset-4"  href="<?php echo base_url(); ?>Productos">Cancelar</a>
              <button type="button" class="btn btn-success guardarform_pro"><?php echo $button;?></button> 
            </div>
          </div>
          <!--------//////////////-------->    
        </div>
        <div class="clearfix"></div>
    </div>
  </div>
  <br />
</div>
<!-- /page content -->