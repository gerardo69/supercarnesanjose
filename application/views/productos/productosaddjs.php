<script src="<?php echo base_url(); ?>public/js/producto.js" type="text/javascript"></script>
<script type="text/javascript">
	<?php if ($img!='') { ?>
		var url1 = '<?php echo base_url().'public/images/productos/'.$img?>';	
	<?php } ?>
	
	$("#exampleInputFile").fileinput({
		<?php if ($img!='') { ?>
    		initialPreview: [url1],
    		initialPreviewAsData: true,
    	<?php } ?>

            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["jpg","jpeg","png"],
            browseLabel: 'Seleccionar imagen',
            //uploadUrl: 'Productos/upload',
            maxFilePreviewSize: 1000,
            allowedPreviewTypes: ['image', 'text'], // allow only preview of image & text files
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            }
        });
</script>