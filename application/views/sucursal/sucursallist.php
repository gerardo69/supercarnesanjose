<style type="text/css">
  .imgpro{height: 100px;border-radius: 5px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);}
  .imgpro:hover{transform: scale(1.5);}
</style>
<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Sucursal <small>Listado</small></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <!--------//////////////-------->
          <div class="row">
            <div class="col-md-8 text-left">
            </div>
            <div class="col-md-4">
              <div class="row text-right">
              </div>           
            </div>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data_tables">
            <thead>
              <tr >
                <th>#</th>
                <th></th>
                <th>Sucursal</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <br />
</div>
