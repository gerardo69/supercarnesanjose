<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Sucursal <small><?php echo $subtitle;?></small></h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Datos generales <small></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form class="form-horizontal form-label-left input_mask" method="post"  role="form" id="formsucursal">
                <input type="text" id="sucursalid" value="<?php echo $sucursalid;?>" name="sucursalid" hidden>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="label-text col-md-3 col-sm-3 col-xs-12">Sucursal</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" id="sucursal" name="sucursal" value="<?php echo $sucursal;?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="label-text col-md-3 col-sm-3 col-xs-12">Dirección</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $direccion;?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="label-text col-md-3 col-sm-3 col-xs-12">Telefono</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $telefono;?>">
                      </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12"></div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                     <input type="file" class="form-control cargaarchivo exampleInputFile" name="logo" id="exampleInputFile">
                    </div>
                </div>
                
              </form>
              <hr>
              <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>Sucursal"><i class="fa fa-reply"></i> Regresar</a>
                    <button type="submit" class="btn btn-success guardarsucursal"><?php echo $button;?></button>
                  </div>
                </div>
            </div>
          </div>

        </div>
    </div>

  </div>
</div>    