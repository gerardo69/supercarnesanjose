<!-- page content -->

<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Usuarios <small></small></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-9 col-xs-12">

          <!--------//////////////---------->
          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
              <table class="table table-striped jambo_table bulk_action" id="data-tables">
                <thead>
                  <tr >
                    <th>Usuario</th>
                    <th>Perfil</th>
                    <th>Sucursal</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($usuarios->result() as $item){ ?>
                    <tr>
                    <td><?php echo $item->Usuario;?></td>
                    <td><?php echo $item->perfil;?></td>
                    <td><?php echo $item->sucursal;?></td>
                    <td>
                      <div class="btn-group">
                      <button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>
                      <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a class="editar_cat" 
                            data-id="<?php echo $item->UsuarioID; ?>" 
                            data-usu="<?php echo $item->Usuario; ?>"
                            data-per="<?php echo $item->personalId; ?>"
                            data-pf="<?php echo $item->perfilId; ?>"
                            >Editar</a>
                        </li>
                        <li>
                          <?php if($item->UsuarioID!=1){?>
                          <a onclick="modal_eliminar(<?php echo $item->UsuarioID; ?>)">Eliminar</a>
                        <?php } ?>
                        </li>
                      </ul>
                    </div>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
              <form autocomplete="off" id="formusuarios">
                <input type="hidden" class="form-control" id="UsuarioID" name="UsuarioID" value="0">
                <div class="row">
                  <label class="label-text col-md-4 col-sm-4 col-xs-12">Nombre de Usuario</label>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" class="form-control" id="Usuario" name="Usuario">
                  </div>
                  
                </div>
                <div class="row">
                  <label class="label-text col-md-4 col-sm-4 col-xs-12">Contraseña</label>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="password" class="form-control" id="contrasena" name="contrasena" autocomplete="new-password">
                  </div>
                </div>
                <div class="row">
                  <label class="label-text col-md-4 col-sm-4 col-xs-12">Validar Contraseña</label>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="password" class="form-control" id="contrasenav" name="contrasenav" autocomplete="new-password">
                  </div>
                </div>
                <div class="row">
                  <label class="label-text col-md-4 col-sm-4 col-xs-12">Empleado</label>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                    <select class="form-control" id="personalId" name="personalId">
                      <?php foreach ($personal->result() as $item){ ?>
                        <option value="<?php echo $item->personalId;?>"><?php echo $item->nombre;?> (<?php echo $item->sucursal;?>)</option>
                      <?php } ?>
                    </select>
                  </div>
                  
                </div>
                <div class="row">
                  <label class="label-text col-md-4 col-sm-4 col-xs-12">Perfil</label>
                  <div class="col-md-8 col-sm-8 col-xs-12">
                    <select class="form-control" id="perfilId" name="perfilId">
                      <?php foreach ($perfiles->result() as $item){ ?>
                        <option value="<?php echo $item->perfilId;?>"><?php echo $item->nombre;?></option>
                      <?php } ?>
                    </select>
                  </div>
                  
                </div>
              </form>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                  <button type="button" class="btn btn-primary btton-cancela">Cancelar</button>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                  <button type="button" class="btn btn-dark btton-save">Guardar</button>
                </div>
              </div>
            </div>
          </div>


          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="modaleliminar">
  <div class="modal-dialog">
    <div class="modal-content curba">

      <div class="modal-header alert-danger curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Confirmación</h3>
      </div>
      <div class="modal-body">
        <h4>¿Está seguro de que desea eliminar el Usuario?</h4>
        <br>
      </div>

      <div class="modal-footer alert-danger curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-info" data-dismiss="modal" onclick="boton_eliminarusu()">Aceptar</button>
      </div>

    </div>
  </div>
</div>  


