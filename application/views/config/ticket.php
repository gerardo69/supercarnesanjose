<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Ticken <small></small></h3>
      </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Configuración <small></small></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form class="form-horizontal form-label-left input_mask" method="post"  role="form" id="formticket">
                <input type="hidden" class="form-control" id="id_ticket" name="id_ticket" value="<?php echo $id_ticket;?>">
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Titulo:</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $titulo;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Mensaje A:</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <textarea class="form-control" id="mensajea" name="mensajea"><?php echo $mensajea;?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Mensaje B:</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <textarea class="form-control" id="mensajeb" name="mensajeb"><?php echo $mensajeb;?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Fuente:</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <select class="form-control" id="fuente" name="fuente">
                      <option value="courier" <?php if($fuente=='courier'){ echo 'selected';}?> >Courier</option>
                      <option value="courierB" <?php if($fuente=='courierB'){ echo 'selected';}?> >Courier Bold</option>
                      <option value="courierBI" <?php if($fuente=='courierBI'){ echo 'selected';}?> >Courier Bold Italic</option>
                      <option value="courierI" <?php if($fuente=='courierI'){ echo 'selected';}?> >Courier Italic</option>
                      <option value="helvetica" <?php if($fuente=='helvetica'){ echo 'selected';}?> >Helvetica</option>
                      <option value="helveticaB" <?php if($fuente=='helveticaB'){ echo 'selected';}?> >Helvetica Bold</option>
                      <option value="helveticaBI" <?php if($fuente=='helveticaBI'){ echo 'selected';}?> >Helvetica Bold Italic</option>
                      <option value="helveticaI" <?php if($fuente=='helveticaI'){ echo 'selected';}?> >Helvetica Italic</option>
                      <option value="symbol" <?php if($fuente=='symbol'){ echo 'selected';}?> >Symbol</option>
                      <option value="times" <?php if($fuente=='times'){ echo 'selected';}?> >Times New Roman</option>
                      <option value="timesB" <?php if($fuente=='timesB'){ echo 'selected';}?> >Times New Roman Bold</option>
                      <option value="timesBI" <?php if($fuente=='timesBI'){ echo 'selected';}?> >Times New Roman Bold Italic</option>
                      <option value="timesI" <?php if($fuente=='timesI'){ echo 'selected';}?> >Times New Roman Italic</option>
                      <option value="zapfdingbats" <?php if($fuente=='zapfdingbats'){ echo 'selected';}?> >Zapf Dingbats</option>

                    </select>
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Tamaño fuente:</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="margen_superior" name="margen_superior" value="<?php echo $margen_superior;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Margen Superior:</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="text" class="form-control" id="tamanio" name="tamanio" value="<?php echo $tamanio;?>">
                  </div>
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Ancho de ticket(mm):</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input type="number" class="form-control" id="anchot" name="anchot" value="<?php echo $anchot;?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="label-text col-md-2 col-sm-2 col-xs-12">Barcode:</label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <select name="barcode" id="barcode" class="form-control">
                      <?php foreach ($barcode->result() as $row) { ?>
                          <option value="<?php echo $row->barId; ?>" <?php if($barcoderow==$row->barId){ echo 'selected';}?> ><?php echo $row->nombre; ?></option>
                      <?php } ?>

                    </select>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 imgbarcode">
                    
                  </div>
                </div>
                <br>
              </form>
              <hr>
              <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <button type="submit" class="btn btn-success guardarticket">Guardar</button>
                  </div>
              </div>
            </div>
          </div>
        </div>
    </div>

  </div>
</div>    