<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Respaldos <small>Listado</small></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-9 col-xs-12">
          <!--------//////////////-------->
          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-primary generar">Generar respaldo</button>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped jambo_table bulk_action" id="data-tables">
            <thead>
              <tr>
                <th>Respaldo</th>
                <th>Creado</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              
              <?php foreach ($total_rows->result() as $item){ ?>
                <tr>
                  <td><?php echo $item->name; ?></td>
                  <td><?php echo $item->nombre; ?></td>
                  <td>
                    <a href="<?php echo base_url();?>uploads/backup/<?php echo $item->name; ?>" class="btn btn-primary" download><i class="fa fa-download"></i></a>
                  </td>
                  

                  
                 

                </tr>
              <?php } ?>
            </tbody>

          </table>
            </div>
          </div>
          
          
          
          

                        

          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>
<!-- /page content -->



