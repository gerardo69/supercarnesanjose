<!--===============================================================================================-->	
	<script src="<?php echo base_url(); ?>public/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>public/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url(); ?>public/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>public/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>public/vendor/tilt/tilt.jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/js/plugins.js"></script>
	<script src="<?php echo base_url(); ?>vendors/formvalidation/formValidation.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>vendors/formvalidation/framework/bootstrap.min.js"></script>
    
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================--
	<script src="<?php echo base_url(); ?>public/js/main.js"></script>-->
	<script type="text/javascript">
		/*
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function() {
        navigator.serviceWorker.register('<?php echo base_url(); ?>service-worker.js')
        .then(function(registration) {
          // Si es exitoso
          console.log('SW registrado correctamente');
        }, function(err) {
          // Si falla
          console.log('SW fallo', err);
        });
      });
    }*/
</script>

</body>
</html>