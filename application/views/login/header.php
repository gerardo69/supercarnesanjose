<!DOCTYPE html>
<html lang="en">
<head>
	<title>BeaverDS POS</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="authoring-tool" content="BeaverDS">
    <meta name="description" content="Punto de venta , BeaverDS">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript,punto de venta,BeaverDS">
    <meta name="author" content="Beaver ds">
    <meta name="og:description" content="Punto de venta , BeaverDS."/>
    <meta name="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="og:title" content="Beaver DS"/>
    <meta name="og:image" content="<?php echo base_url(); ?>public/images/favicon.png"/>

    <link rel="icon" href="<?php echo base_url(); ?>public/images/favicon.ico?v=2018082101" type="image/x-icon" />
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>public/images/favicon.png" type="image/png" />
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>public/images/favicon.png" type="image/png" />
    <link rel="apple-touch-icon" sizes="194x194" href="<?php echo base_url(); ?>public/images/favicon.png" type="image/png" />
    <meta name="theme-color" content="#2c98bb">
<!--===============================================================================================--	
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/util.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendors/formvalidation/formValidation.min3f0d.css?v2.2.0">
	<link rel="manifest" href="<?php echo base_url(); ?>manifest.json">
	<style type="text/css">
		.success{
			background: #26B99A;
		}
		.danger{
			background: #ac2925;
		}
	</style>
<!--===============================================================================================-->
</head>
<body>