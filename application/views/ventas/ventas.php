<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/ventas.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/ticket.css" media="print">
<style type="text/css">
  .iframeprint{
    width: 100%;
    height: 76vh;
    border: 0;
  }
  .iframeticket{
    padding: 0px;
  }

</style>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <!--<h3>Proveedor</h3>-->
        <input type="hidden" name="fecha_actual" id="fecha_actual" value="<?php echo $fecha_actual?>">
        <input type="hidden" name="fecha_vigencia" id="fecha_vigencia" value="<?php echo $fecha_vigencia?>">
        <input type="hidden" name="fecha_v" id="fecha_v" value="<?php echo $fecha_v?>">
        <input type="hidden" name="perfilid" id="perfilid" value="<?php echo $perfilid?>">
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 versiontouch">
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <!--<h2>Datos generales <small></small></h2>-->
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <!--//////////////////////-->
            <div class="row">
              <div class="col-md-6 col-xs-6">
                <input type="checkbox" name="checkimprimir" id="checkimprimir" checked> <label for="checkimprimir"> Imprimir Ticket</label>
              </div>
              <div class="col-md-6 col-xs-6 text-right">
                <button type="button" class="btn btn-primary calculadoramodal"><i class="fa fa-calculator"></i></button>  
                <button type="button" class="btn btn-primary fullscreen" onclick="fullscreen()"><i class="fa fa-arrows-alt"></i></button>   
                <button type="button" class="btn btn-primary productostactiles">Selección táctil</button>          
              </div>
              
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="row">
                  <label class="control-label col-md-3 col-xs-3">Cliente</label>
                  <div class="col-md-9 col-xs-9">
                    <select class="form-control" id="clientes">
                      <option value="1">Publico General</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="control-label col-md-3 col-xs-3">
                    
                  </div>
                  <div class="col-md-9 col-xs-9">
                    <label class="control-label col-md-12 col-xs-12"><i class="fa fa-search"></i> Buscar producto/Codigo:</label>
                    <select class="form-control" id="producto"></select>
                  </div>
                </div>
                <div class="row">
                  <div class="control-label col-md-3 col-xs-3">
                    <label>Cantidad</label>
                    <input type="number" id="cantidad" value="1" class="form-control">
                  </div>
                  <div class="col-md-9 col-xs-9">
                    <label class="control-label col-md-12 col-xs-12"><i class="fa fa-barcode"></i> Producto/Codigo:</label>
                    <input type="text" class="form-control" id="producto2">
                  </div>
                </div>
                <div class="row"><br><br><br></div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <table class="table table-hover" id="productosv">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>Precio Unitario</th>
                          <th>Precio Total</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="class_productos">
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-md-4  iframereportex" style="display: none;"></div>
              
            </div>
            <hr>
            <!--//////////////////////-->
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12">
                <button type="button" class="btn btn-info productosclear">Limpiar</button>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <button type="button" class="btn btn-success guardarventa">Realizar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <!--<h2>Datos generales <small></small></h2>-->
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <!--//////////////////////-->
            <div class="row">
              <div class="form-group" <?php echo $list_block;?> >
                  <label class="label-text col-md-5 col-sm-5 col-xs-12">Sucursal</label>
                  <div class="col-md-7 col-sm-9 col-xs-12">
                     <select name="sucursalselected" class="form-control " id="sucursalselected">
                        <?php foreach ($sucursalesrow->result() as $row) { ?>
                            <option value="<?php echo $row->sucursalid;?>"><?php echo $row->sucursal?></option>
                        <?php } ?>
                      </select>
                      <span class="fa fa-building-o form-control-feedback left" aria-hidden="true"></span>
                  </div>
              </div>
            </div>
            
            <div class="row">
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Tipo de pago</label>
              <label class="label-text col-md-3 col-sm-3 col-xs-12">
                <input type="radio" name="tipopago" value="1" checked>
              Contado</label>
              <label class="label-text col-md-3 col-sm-3 col-xs-12">
                <input type="radio" name="tipopago" value="2">
              Crédito</label>
            </div>
            <div class="row metodocontado">
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Método de pago</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <select class="form-control" name="metodo" id="metodo">
                  <option value="1">Efectivo</option>
                  <option value="2">Tarjeta de crédito</option>
                  <option value="3">Tarjeta de débito</option>
                </select>
              </div>
            </div>
            <div class="row metodocredito" style="display: none;">
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Fecha de vencimiento</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="date" name="fechavencimiento" id="fechavencimiento" class="form-control">
              </div>
            </div>
            <div class="row">
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Subtotal</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="number" name="subtotal" id="subtotal" value="0" class="form-control" readonly style="font-weight: bold; background: transparent; border:0px;">
              </div>
              
            </div>
            <div class="row">
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Descuento</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <select class="form-control" name="ndescuento" id="ndescuento">
                  <option value="0">0 %</option>
                  <option value="0.05">5 %</option>
                  <option value="0.1">10 %</option>
                  <option value="0.15">15 %</option>
                  <option value="0.2">20 %</option>
                  <option value="0.3">30 %</option>
                </select>
              </div>
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Monto Descuento</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="number" name="descuento" id="descuento" value="0" class="form-control" readonly style="background: transparent; border:0px;">
              </div>
            </div>
            <div class="row">
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Total</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="text" name="totalinfo" id="totalinfo" value="0" class="form-control" readonly style="font-weight: bold; font-size: 20px;background: transparent; border:0px;">
                <input type="hidden" name="total" id="total" value="0" class="form-control" readonly>
              </div>
            </div>
            <div class="row">
              <hr>
            </div>

            <div class="row">
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Pago</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="number" name="pago" id="pago" value="0" class="form-control" min="0" oninput="calcular();" >

              </div>
            </div>
            <div class="row">
              <label class="label-text col-md-5 col-sm-5 col-xs-12">Cambio</label>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <input type="number" name="cambio" id="cambio" value="0" class="form-control" min="0" readonly>

              </div>
            </div>
            
            <!--//////////////////////-->
            
          </div>
        </div>
      </div>
      

    </div>
    <div class="row datosticket" style="display: none;"></div>
  </div>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="modalcalculadora">
  <div class="modal-dialog">
    <div class="modal-content curba">

      <div class="modal-header  curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Calculadora</h3>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="content">
                <div class="calculator">
                    <div class="screen"></div>
                    <input type="hidden" value="" class="outcome" />
                    <ul class="buttons">
                        <li class="clear"><a>C</a></li>
                        <li><a href="-" class="val">&plusmn;</a></li>
                        <li><a href="/" class="val">&divide;</a></li>
                        <li><a href="*" class="val">&times;</a></li>    
                        <li><a href="7" class="val">7</a></li>
                        <li><a href="8" class="val">8</a></li>
                        <li><a href="9" class="val">9</a></li>
                        <li><a href="-" class="val">-</a></li>
                        <li><a href="4" class="val">4</a></li>
                        <li><a href="5" class="val">5</a></li>
                        <li><a href="6" class="val">6</a></li>
                        <li><a href="+" class="val">+</a></li>
                        <li><a href="1" class="val">1</a></li>
                        <li><a href="2" class="val">2</a></li>
                        <li><a href="3" class="val">3</a></li>
                        <li><a class="equal tall">=</a></li>
                        <li><a href="0" class="val wide shift">0</a></li>
                        <li><a href="." class="val shift">.</a></li>
                    </ul>
                </div>
            </div>
          </div>
        </div> 
      </div>
      <div class="modal-footer  curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>  
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="modal_print">
  <div class="modal-dialog">
    <div class="modal-content curba">

      <div class="modal-header curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Impresion</h3>
      </div>
      <div class="modal-body iframereporte">
        
      </div>
      
      <div class="modal-footer  curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-default" onclick="imprimiriframa('iframeprint')">Imprimir</button>      
      </div>

    </div>
  </div>
</div>  

