<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Sistema';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//========== consultas nuevo y excistentes ==============
	$route['Personal/Personaladd/(:num)'] = 'Personal/Personaladd/$1';//cuando sea nuevo y edicion
	$route['Productos/Productosadd/(:num)'] = 'Productos/Productosadd/$1';//cuando sea nuevo y edicion
	$route['Clientes/Clienteadd/(:num)'] = 'Clientes/Clienteadd/$1';//cuando sea nuevo y edicion
	$route['Proveedores/Proveedoresadd/(:num)'] = 'Proveedores/Proveedoresadd/$1';//cuando sea nuevo y edicion
	$route['Sucursal/Sucursaladd/(:num)'] = 'Sucursal/Sucursaladd/$1';//cuando sea nuevo y edicion
	$route['Reportes/ticket_venta/(:num)/(:num)'] = 'Reportes/ticket_venta/$1/$2';//cuando sea nuevo y edicion
//=======================================================

//========== paginacion ===================
	$route['Personal/view/(:num)'] = 'Personal';//cuando no sea la primera página
	$route['Personal/view'] = 'Personal';

	$route['Productos/view/(:num)'] = 'Productos';//cuando no sea la primera página
	$route['Productos/view'] = 'Productos';

	$route['Clientes/view/(:num)'] = 'Clientes';//cuando no sea la primera página
	$route['Clientes/view'] = 'Clientes';

	$route['Proveedores/view/(:num)'] = 'Proveedores';//cuando no sea la primera página
	$route['Proveedores/view'] = 'Proveedores';

	$route['Sucursal/view/(:num)'] = 'Sucursal';//cuando no sea la primera página
	$route['Sucursal/view'] = 'Sucursal';

	$route['ListadoVentas/view/(:num)'] = 'ListadoVentas';//cuando no sea la primera página
	$route['ListadoVentas/view'] = 'ListadoVentas';

	$route['ListadoVentasNormales/view/(:num)'] = 'ListadoVentasNormales';//cuando no sea la primera página
	$route['ListadoVentasNormales/view'] = 'ListadoVentasNormales';

	$route['ListadoVentasCredito/view/(:num)'] = 'ListadoVentasCredito';//cuando no sea la primera página
	$route['ListadoVentasCredito/view'] = 'ListadoVentasCredito';

	$route['Compras/view/(:num)'] = 'Compras';//cuando no sea la primera página
	$route['Compras/view'] = 'Compras';

	$route['Reportes'] = 'Corte';
//========================================



