var base_url=$('#base_url').val();
$(document).ready(function($) {
  $('.guardarsucursal').click(function(event) {
    var data = new FormData();
    if ($('#exampleInputFile')[0].files.length > 0) {
      var inputFileImage = document.getElementById('exampleInputFile');
      var file = inputFileImage.files[0]; 
      data.append('logo',file);
    }
    data.append('sucursalid',$('#sucursalid').val());
    data.append('sucursal',$('#sucursal').val());
    data.append('direccion',$('#direccion').val());
    data.append('telefono',$('#telefono').val());
    $.ajax({
      url:base_url+'Sucursal/add',
      type:'POST',
      contentType:false,
      data:data,
      processData:false,
      cache:false,
      success: function(data) {
        console.log(data);
        new PNotify({
          title: 'Hecho!',
          text: 'Guardado Correctamente',
          type: 'success',
          styling: 'bootstrap3'
        });
        setInterval(function(){ 
          location.href=base_url+'Sucursal';
        }, 3000);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        var data = JSON.parse(jqXHR.responseText);
        console.log(data);
        if (data.ok=='true') {
          $(".fileinput").fileinput("clear");
        }else{
          toastr.error('Error', data.msg);
        }

      }
    });
  });   
});