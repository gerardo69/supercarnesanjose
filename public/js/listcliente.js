var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
	loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"Clientes/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "ClientesId"},  
            {"data": "Nombre"}, 
            {"data": "Domicilio"},
            {"data": "Correo"}, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='';
                    if (row.nombrec!='') {
                      var html='<h6><i class="fa fa-user dccliente"></i> '+row.nombrec+'</h6>\
                              <h6><i class="fa fa-envelope dccliente"></i> '+row.correoc+'</h6>\
                              <h6><i class="fa fa-phone  dccliente"></i> '+row.telefonoc+'</h6>';
                    }
                    
                    return html;
                }
            }, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='<div class="btn-group">\
                                <button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>\
                                <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">\
                                  <span class="caret"></span>\
                                  <span class="sr-only">Toggle Dropdown</span>\
                                </button>\
                                <ul class="dropdown-menu" role="menu">\
                                  <li>\
                                    <a href="'+base_url+'Clientes/Clienteadd/'+row.ClientesId+'">Editar</a>\
                                  </li>\
                                  <li>\
                                    <a class="clien_'+row.ClientesId+'" data-cliente="'+row.Nombre+'" onclick="modal_eliminar('+row.ClientesId+')">Eliminar</a>\
                                  </li>\
                                </ul>\
                              </div>';
                    return html;
                }
            }, 
        ],
        order: [
            [0, "desc"]
        ],
    });
}
function reload_tabla(){
    table.destroy();
    loadtable();
}
function modal_eliminar(id){
    $('#eliminar_modal').modal();
    $("#ClientesId").val(id);
    var nombre=$('.clien_'+id).data('cliente');
    $(".nom").html('<b>'+nombre+'</b>');
}
function boton_eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Clientes/eliminar',
        data: {ClientesId:$("#ClientesId").val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            new PNotify({
                      title: 'Hecho!',
                       text: 'Eliminado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
            reload_tabla();
        }
    });
}