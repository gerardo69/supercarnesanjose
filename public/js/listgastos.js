var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
	loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"Gastos/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "gastosid"},  
            {"data": "fecha"}, 
            {"data": "cantidad"},
            {"data": "concepto"}, 
            {"data": "nombre"},  
            {"data": "sucursal"}, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='';
                    if(row.activo==0){
                         html='<button class="btn btn-dark float-right bttonsttuscancelado gastoc_'+row.gastosid+'"\
                            data-cpersonal="'+row.personalc+'"\
                            data-cmotivo="'+row.motivo_cancela+'"\
                            data-cfecha="'+row.reg_cancela+'"\
                            onclick="modal_cancelados('+row.gastosid+')">Cancelado</button>'
                    }
                    return html;
                }
            }, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='';
                    if (row.activo!=0){
                        html='<button type="button" class="btn btn-round btn-danger btn-sm" onclick="cancelar_modal('+row.gastosid+')">Cancelar</button>';
                    }
                    return html;
                }
            }, 
        ],
        order: [
            [0, "desc"]
        ],
    });
}
function reload_tabla(){
    table.destroy();
    loadtable();
}
function cancelar_modal(id){
    $('#cancelar_modal').modal();
    $('#idgasto').val(id);
}
function modal_cancelados(id){
    $('#cancelado_modal').modal();
    $('#cper').val($('.gastoc_'+id).data("cpersonal"));
    $('#mot').val($('.gastoc_'+id).data("cmotivo"));
    $('#fec').val($('.gastoc_'+id).data("cfecha"));  
}
function btn_cancelar(){
    $.ajax({ 
        type:'POST',
        url: base_url+'Gastos/cancelar',
        data: {idgasto:$("#idgasto").val(),motivo_cancela:$('#motivo_cancela').val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
             new PNotify({
                      title: 'Hecho!',
                       text: 'Cancelado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
            reload_tabla();
            
        }
    });
}
