var base_url = $('#base_url').val();
var tabledata;
$(document).ready(function($) {
    loadtable();
});
function modal_eliminar(id, name) {
    $('#eliminar_modal').modal();
    // $(".eliminar_modal").slideToggle()
    $("#productoid").val(id);
    $(".nom").html('<b>' + name + '</b>');
    
}
function recargartable(){
    tabledata.destroy();
    loadtable();
}
function boton_eliminar() {
    $.ajax({
        type: 'POST',
        url: base_url + 'Productos/eliminar',
        data: {
            productoid: $("#productoid").val()
        },
        async: false,
        statusCode: {
            404: function(data) {
                new PNotify({
                    title: 'Error!',
                    text: 'No se encuentra archivo',
                    type: 'error',
                    styling: 'bootstrap3'
                });

            },
            500: function() {
                new PNotify({
                    title: 'Error!',
                    text: '500',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        },
        success: function(data) {
            new PNotify({
                title: 'Hecho!',
                text: 'Eliminado Correctamente',
                type: 'success',
                styling: 'bootstrap3'
            });
            setTimeout(function() {
                tabledata.destroy();
                loadtable();
            }, 1000);

        }
    });
}

function loadtable() {
    var sucursal=$('#sucursalselected option:selected').val();
  var stksuc = $('#stksuc').val();
  var stkper = $('#stkper').val();

  
    tabledata = $('#data-tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Productos/getData_producto",
            type: "post",
            "data": {
                "sucu": sucursal
            },
            error: function() {
                $("#tabla_producto").css("display", "none");
            }
        },
        "columns": [
            {"data": "productoid"}, 
            {"data": null,
                "render": function(url, type, full) {
                    var html = '';
                    if (full['img'] == '') {
                        var img = 'public/images/pos.svg';
                    } else {
                        var img = 'public/images/productost/' + full['img'];
                    }
                    html += '<img src="' + base_url + '' + img + '" class="imgpro">';

                    return html;
                }}, 
            {"data": "codigo"}, 
            {"data": "nombre"}, 
            {"data": "categoria"}, 
            {"data": "existencia"}, 
            {"data": "precio_venta"}, 
            {"data": null,
            "render": function(url, type, full) {
                var idpro = full['productoid'];
                var idproname = "'" + full['nombre'] + "'";
                var html = '';

                html += '<div class="btn-group">';
                html += '<button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>';
                html += '<button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
                html += '<span class="caret"></span>';
                html += '<span class="sr-only">Toggle Dropdown</span>';
                html += '</button>';
                html += '<ul class="dropdown-menu" role="menu">';
                html += '<li>';
                html += '<a href="' + base_url + 'Productos/Productosadd/' + idpro + '">Editar</a>';
                html += '</li>';
                html += '<li>';
                html += '<a onclick="modal_eliminar(' + idpro + ',' + idproname + ')">Eliminar</a>';
                html += '</li>';
                html += '</ul>';
                html += '</div>';

                return html;
            }
          }, ],
        order: [
            [0, "desc"]
          ],
        dom: 'Bfrtip',
                                buttons: [
                                {extend: "excel",className: "btn-sm"}, 
                                {extend: "pdfHtml5",className: "btn-sm"}, 
                                {extend: "print",className: "btn-sm"},
                                'pageLength'
                                ],
    });
    //tabledata.columns( columshidden ).visible( false );
    /*
    tabledata = $('#data-tables').DataTable();
    */
}