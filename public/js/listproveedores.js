var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
	loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"Proveedores/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "id_proveedor"},
            {"data": "razon_social"},
            {"data": "domicilio"},
            {"data": "ciudad"}, 
            {"data": "telefono_local"}, 
            {"data": "telefono_celular"}, 
            {"data": "email_contacto"}, 
            {"data": "rfc"},  
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='<div style="width:64px">\
                                <div class="btn-group">\
                                  <button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>\
                                  <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">\
                                    <span class="caret"></span>\
                                    <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <ul class="dropdown-menu" role="menu">\
                                    <li>\
                                      <a href="'+base_url+'Proveedores/Proveedoresadd/'+row.id_proveedor+'">Editar</a>\
                                    </li>\
                                    <li>\
                                      <a class="provee_'+row.id_proveedor+'" data-proveedor="'+row.razon_social+'" onclick="modal_eliminar('+row.id_proveedor+')">Eliminar</a>\
                                    </li>\
                                  </ul>\
                                </div>\
                            </div>';
                    
                    return html;
                }
            }, 
        ],
        order: [
            [0, "desc"]
        ],
    });
}
function reload_tabla(){
    table.destroy();
    loadtable();
}
function modal_eliminar(id){
    $('#eliminar_modal') .modal();
    $("#id_proveedor").val(id);
    var nombre=$('.provee_'+id).data('proveedor');
    $(".nom").html('<b>'+nombre+'</b>');
}
function boton_eliminar(){
    $.ajax({ 
        type:'POST',
        url: base_url+'Proveedores/eliminar',
        data: {id_proveedor:$("#id_proveedor").val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            new PNotify({
                title: 'Hecho!',
                text: 'Eliminado Correctamente',
                type: 'success',
                styling: 'bootstrap3'
            });
            reload_tabla();  
        }
    });
}