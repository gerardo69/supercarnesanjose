var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
	loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"Sucursal/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "sucursalid"},  
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='<img src="'+base_url+'public/images/sucursalt/'+row.logo+'" class="imgpro">';
                    return html;
                }
            }, 
            {"data": "sucursal"}, 
            {"data": "direccion"},
            {"data": "telefono"}, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='<a class="btn btn-round btn-success btn-sm" title="Editar" href="'+base_url+'Sucursal/Sucursaladd/'+row.sucursalid+'"><i class="fa fa-edit"></i></a>';
                    return html;
                }
            }, 
        ],
        order: [
            [0, "desc"]
        ],
    });
}