var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('.btnBuscar').click(function(event) {
		$.ajax({
	        type: 'POST',
	        url: base_url + 'Corte/reportes',
	        data: {
	            fechaini: $('#fechaini').val(),
	            fechafin: $('#fechafin').val(),
	            tipo:$('#tipo option:selected').val()
	        },
	        async: false,
	        statusCode: {
	            404: function(data) {
	                new PNotify({
	                    title: 'Error!',
	                    text: 'No Se encuentra el archivo',
	                    type: 'error',
	                    styling: 'bootstrap3'
	                });
	            },
	            500: function() {
	                new PNotify({
	                    title: 'Error!',
	                    text: 'Error 500',
	                    type: 'error',
	                    styling: 'bootstrap3'
	                });
	            }
	        },
	        success: function(data) {
	            $('.addinforme').html(data);
	           

	            $('#data-tables').DataTable({
	            	fixedHeader: !0,
                                dom: 'Bfrtip',
                                buttons: [
                                {extend: "copy", className: "btn-sm"}, 
                                {extend: "csv",className: "btn-sm"}, 
                                {extend: "excel",className: "btn-sm"}, 
                                {extend: "pdfHtml5",className: "btn-sm"}, 
                                {extend: "print",className: "btn-sm"},
                                'pageLength'
                                ],
			                responsive: !0
			                            });
	            $('#data-tables2').DataTable({
	            	fixedHeader: !0,
                                dom: 'Bfrtip',
                                buttons: [
                                {extend: "copy", className: "btn-sm"}, 
                                {extend: "csv",className: "btn-sm"}, 
                                {extend: "excel",className: "btn-sm"}, 
                                {extend: "pdfHtml5",className: "btn-sm"}, 
                                {extend: "print",className: "btn-sm"},
                                'pageLength'
                                ],
			                responsive: !0
			                            });


	        }
	    });
	});
	$('.imprimir').click(function(event) {
		window.print();
	});
	$('#tipo').change(function(event) {
		var tipo = $('#tipo option:selected').val();
		if (tipo==5) {
			$('#fechaini').attr('readonly', true);
			$('#fechafin').attr('readonly', true);
		}else{
			$('#fechaini').attr('readonly', false);
			$('#fechafin').attr('readonly', false);
		}

	});
    
});