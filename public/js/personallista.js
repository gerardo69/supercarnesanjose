var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
	loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"Personal/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "personalId"},  
            {"data": "nombre"}, 
            {"data": "correo"},
            {"data": "celular"}, 
            {"data": "sucursal"},
            {"data": null,
            "render": function(data, type, row, meta ) {
                var html='<div class="btn-group">\
                      <button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>\
                      <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">\
                        <span class="caret"></span>\
                        <span class="sr-only">Toggle Dropdown</span>\
                      </button>\
                      <ul class="dropdown-menu" role="menu">\
                        <li>\
                          <a href="'+base_url+'Personal/Personaladd/'+row.personalId+'">Editar</a>\
                        </li>\
                        <li>\
                          <a class="emple_'+row.personalId+'" data-empleado="'+row.nombre+'" onclick="modal_eliminar('+row.personalId+')">Eliminar</a>\
                        </li>\
                      </ul>\
                    </div>';

                    return html;
                }
          }, ],
        order: [
            [0, "desc"]
        ],
    });
}
function reload_tabla(){
    table.destroy();
    loadtable();
}
function modal_eliminar(id){
    $('#eliminar_modal') .modal();
    $("#personalId").val(id);
    var nombre=$('.emple_'+id).data('empleado'); 
    $(".nom").html('<b>'+nombre+'</b>');
}
function boton_eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Personal/eliminar',
        data: {personalId:$("#personalId").val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            new PNotify({
                      title: 'Hecho!',
                       text: 'Eliminado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
            reload_tabla();
        }
    });
}