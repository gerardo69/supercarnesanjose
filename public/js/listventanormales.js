var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
    loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"ListadoVentasNormales/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "ventaId"},  
            {"data": "cliente"}, 
            {"data": "subtotal"},
            {"data": "ndescuento"}, 
            {"data": "descuento"},  
            {"data": "total"}, 
            {"data": "reg"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='';
                    if (row.cancelado==1){  
                        html='<button class="btn btn-danger float-right detales_ventan_'+row.ventaId+'"\
                                 data-cpersonal="'+row.nombre+'"\
                                 data-cmotivo="'+row.cancela_motivo+'"\
                                 data-cfecha="'+row.canceladoh+'"\
                                onclick="bttonsttuscancelado('+row.ventaId+')">Cancelado</button>';
                    }
                    return html;
                }
            }, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var btn_canelar='';
                    if(row.cancelado != 1){
                        btn_canelar='<button type="button" class="btn btn-dark" onclick="modal_cancela('+row.ventaId+')">Cancelar</button>';
                    }  
                    var html=btn_canelar+'\
                        <a class="btn btn-default" href="'+base_url+'Reportes/ticket_venta/'+row.ventaId+'" target="_blank" title="Ticket"><i class="fa fa-ticket"></i></a>';
                    return html;
                }
            }, 
        ],
        order: [
            [0, "desc"]
        ],
    });
}
function reload_tabla(){
    table.destroy();
    loadtable();
}
function bttonsttuscancelado(id){
  $('#cancelado_modal').modal();
  $('#cper').val($('.detales_ventan_'+id).data("cpersonal"));
  $('#mot').val($('.detales_ventan_'+id).data("cmotivo"));
  $('#fec').val($('.detales_ventan_'+id).data("cfecha")); 
}
/// Modal de cancelacion
function modal_cancela(id){
  $('#cancelar_modal').modal();
  $('#ventaId_venta').val(id);
}  

function btn_cancelar(){
  $.ajax({ 
        type:'POST',
        url: base_url+'ListadoVentasNormales/cancelar',
        data: {ventaId:$("#ventaId_venta").val(),cancela_motivo:$('#cancela_motivo').val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
              new PNotify({
                      title: 'Hecho!',
                       text: 'Guardado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
            reload_tabla();
        }
    });
}
