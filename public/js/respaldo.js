var base_url=$('#base_url').val();

$(document).ready(function($) {
	$('.generar').click(function(event) {
		$.ajax({
            type:'POST',
            url: base_url+'Respaldos/generar',
            async: false,
            statusCode:{
                404: function(data){
                    new PNotify({
                                  title: 'Error!',
                                  text: 'No se encuentra archivo',
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });
                },
                500: function(){
                     new PNotify({
                                  title: 'Error!',
                                  text: '500',
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });
                }
            },
            success:function(data){
              var array = $.parseJSON(data);
                new PNotify({
                                  title: array.title,
                                  text: array.text,
                                  type: array.type,
                                  styling: 'bootstrap3'
                              });
                if (array.msj==1) {
                  setInterval(function(){ 
                            location.href='';
                  }, 3000);
                }
                

                
                
            }
        });
	});	
	

});


