var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
  loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"ListadoVentasCredito/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "ventaId"},  
            {"data": "cliente"}, 
            {"data": "subtotal"},
            {"data": "ndescuento",
                "render": function(data, type, row, meta ) {
                  var html='';
                    var descuento=parseFloat(row.ndescuento)*100;
                    html +=descuento+' %'
                  return html;
                }
            }, 
            {"data": "descuento"},  
            {"data": "total"}, 
            {"data": "fechavencimiento"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='';
                    if(row.pagado!=0){  
                      html='<a class=" btn-success btn-sm">Cubierto</a>';
                    }else{
                      html='<a class=" btn-danger btn-sm">No cubierto</a>';
                    }
                    return html;
                }
            }, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='';
                    if (row.cancelado==1){  
                        html='<button class="btn btn-dark float-right detales_ventan_'+row.ventaId+'"\
                                 data-cpersonal="'+row.nombre+'"\
                                 data-cmotivo="'+row.cancela_motivo+'"\
                                 data-cfecha="'+row.canceladoh+'"\
                                onclick="bttonsttuscancelado('+row.ventaId+')">Cancelado</button>';       
                    }
                    return html;
                }
            }, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='<div style="width:64px"><div class="btn-group">\
                                  <button type="button" class="btn btn-dark"> <i class="fa fa-cog"></i> </button>\
                                  <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">\
                                    <span class="caret"></span>\
                                    <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <ul class="dropdown-menu" role="menu">\
                                        <li>\
                                          <a href="'+base_url+'Reportes/ticket_venta/'+row.ventaId+'" target="_blank">Ticket</a>\
                                        </li>';
                                      if(row.cancelado!=1){
                                          if(row.pagado==0){
                                        html+='<li><a onclick="modal_pago('+row.ventaId+','+row.total+')">Agregar pago</a></li>\
                                            <li><a onclick="modal_cancela('+row.ventaId+')">Cancelar</a></li>';
                                          } 
                                        }
                            html+='</ul>\
                              </div></div>';

                    return html;
                }
            }, 
        ],
        order: [
            [0, "desc"]
        ],
    });
}
function reload_tabla(){
    table.destroy();
    loadtable();
}
function bttonsttuscancelado(id){
    $('#cancelado_modal').modal();
    $('#cper').val($('.detales_ventan_'+id).data("cpersonal"));
    $('#mot').val($('.detales_ventan_'+id).data("cmotivo"));
    $('#fec').val($('.detales_ventan_'+id).data("cfecha"));  
}
function modal_pago(id){
	$('#pago_modal').modal();
	$('#ventaId').val(id);
  $.ajax({ 
        type:'POST',
        url: base_url+'ListadoVentasCredito/pagos',
        data: {ventaId:id},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
              $('.tabla_pagos').html(data); 
        }
    });
}
function btn_agregar(){
  var pago = $('#pago').val();
  if(pago!=''){
      $.ajax({ 
        type:'POST',
        url: base_url+'ListadoVentasCredito/add',
        data: {ventaId:$("#ventaId").val(),pago:$('#pago').val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
             if(data == 1){
                new PNotify({
                      title: 'Verificar!',
                       text: 'Es mayor a la cantidad que debe',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
             }else{
              new PNotify({
                      title: 'Hecho!',
                       text: 'Guardado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
             setTimeout(function(){ 
                 $('#pago_modal').modal('hide');
                }, 1000);
             }
             reload_tabla();
        }
    });
  }else{
    new PNotify({
        title: 'Verificar!',
         text: 'Ingrese una cantidad',
        type: 'error',
        styling: 'bootstrap3'
    });
  }
	
}
/// Modal de cancelacion
function modal_cancela(id){
  $('#cancelar_modal').modal();
  $('#ventaId_venta').val(id);
}  

function btn_cancelar(){
  $.ajax({ 
        type:'POST',
        url: base_url+'ListadoVentasCredito/cancelar',
        data: {ventaId:$("#ventaId_venta").val(),cancela_motivo:$('#cancela_motivo').val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            new PNotify({
              title: 'Hecho!',
               text: 'Guardado Correctamente',
              type: 'success',
              styling: 'bootstrap3'
            });
            reload_tabla();
        }
    });
}
