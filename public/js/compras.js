var base_url = $('#base_url').val();
$(document).ready(function() {
    $('#id_proveedor').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Proveedor',
        ajax: {
            url: base_url + 'General/searchproveedor',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var clientes = data;
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id_proveedor,
                        text: element.razon_social
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('#producto').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        ajax: {
            url: base_url + 'General/searchproducto',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var clientes = data;
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.productoid,
                        text: element.nombre,
                        precio: element.preciocompra
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        $('#precioc').val(data.precio);
    });
    var productocelected=0;
    $('.addproducto').click(function(event) {
        var cantidad = $('#cantidadp').val();
        cantidad == '' ? 0 : cantidad;
        if (cantidad > 0) {
        	var producto = $('#producto option:selected').val();
        	if (producto!=null) {

        		var precio =$('#precioc').val();
        		var productotext = $('#producto option:selected').text();
        		var totalpro=parseFloat(cantidad) * parseFloat(precio);

        	 	var productoadd='<tr class="producto_'+productocelected+'">';
        	     	productoadd+='<td><input type="hidden" name="productoid" id="productoid" value="'+producto+'">'+productotext+'</td>';
        	     	productoadd+='<td><input type="number" name="cantidad" id="cantidad" value="'+cantidad+'" readonly style="background: transparent;border: 0px;width: 80px;"></td>';
        	     	productoadd+='<td>$ <input type="text" name="precio_compra" id="precio_compra" value="'+precio+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>';
        	     	productoadd+='<td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'+totalpro+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>';                                        
        	     	productoadd+='<td><button type="button" class="btn btn-danger" title="Eliminar" onclick="deletepro('+productocelected+')"><i class="fa fa-minus-circle"></i> </button></td></tr>';
	                $('#class_productos').append(productoadd);
	                calculartotal();
	                productocelected++;
	        }else{
	        	new PNotify({
	                title: 'Advertencia!',
	                text: 'Debe de seleccionar un producto',
	                type: 'error',
	                styling: 'bootstrap3'
	            });
	        }

        } else {
            new PNotify({
                title: 'Advertencia!',
                text: 'Debe de agregar una cantidad mayor a 0',
                type: 'error',
                styling: 'bootstrap3'
            });
        }
    });
    $('.limpiar').click(function(event) {
    	$('#class_productos').html('');
		calculartotal();
    });
    $('.guardar').click(function(event) {
    	var table = $("#productosc tbody > tr");
    	if (table.length>0) {
    		var id_proveedor = $('#id_proveedor option:selected').val();
    		if (id_proveedor!=null) {
    			$.ajax({
	                type: 'POST',
	                url: base_url + 'Compras/ingresarcompra',
	                data: {
	                    id_proveedor: $('#id_proveedor option:selected').val(),
                        sucursalid: $('#sucursalselected option:selected').val(),
	                    monto_total: $('#monto_total').val()
	                },
	                async: false,
	                statusCode: {
	                    404: function(data) {
	                        new PNotify({
	                            title: 'Error!',
	                            text: 'No Se encuentra el archivo',
	                            type: 'error',
	                            styling: 'bootstrap3'
	                        });
	                    },
	                    500: function() {
	                        new PNotify({
	                            title: 'Error!',
	                            text: 'Error 500',
	                            type: 'error',
	                            styling: 'bootstrap3'
	                        });
	                    }
	                },
	                success: function(data) {
	                	console.log(data);
	                    addproductoscompras(data);
	                }
	            });	
    		}else{
    			new PNotify({
	                            title: 'Error!',
	                            text: 'Seleccione un proveedor',
	                            type: 'error',
	                            styling: 'bootstrap3'
	                        });
    		}
    		

    	}else{
    		new PNotify({
	                title: 'Advertencia!',
	                text: 'Debe de agregar un producto',
	                type: 'error',
	                styling: 'bootstrap3'
	            });
    	}
    });
});
function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    
    $('#monto_total').val(Number(addtp).toFixed(2));
}
function deletepro(id){
	$('.producto_'+id).remove();
	calculartotal();
}
function addproductoscompras(idcompra){
	var DATA = [];
    var TABLA = $("#productosc tbody > tr");
    TABLA.each(function() {
        item = {};
        item["compraId"] = idcompra;
        item["productoid"] = $(this).find("input[id*='productoid']").val();
        item["cantidad"] = $(this).find("input[id*='cantidad']").val();
        item["precio_compra"] = $(this).find("input[id*='precio_compra']").val();
        item["sucursalid"] = $('#sucursalselected option:selected').val();
        DATA.push(item);
    });
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Compras/ingresarcomprapro',
        processData: false,
        contentType: false,
        async: false,
        statusCode: {
            404: function(data) {

                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
        },
        success: function(data) {
        	new PNotify({
                                  title: 'Hecho!',
                                  text: 'Guardado Correctamente',
                                  type: 'success',
                                  styling: 'bootstrap3'
                              });
            checkprint = document.getElementById("checkimprimir").checked;
            if (checkprint == true) {
                $('#modal_print').modal();
                var cambio = $('#cambio').val();
                $('.iframereporte').html('<iframe src="' + base_url + 'Reportes/ticket_compras/' + idcompra + '" class="iframeprint" id="iframeprint"></iframe>');
            }
            setTimeout(function(){ 
                $('#class_productos').html('');
                calculartotal();
            }, 2000);

        }
    });
}
function imprimiriframa(documentId){
    document.getElementById(documentId).contentWindow.print();
}