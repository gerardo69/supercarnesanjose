var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
  loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"Compras/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "compraId"},  
            {"data": "razon_social"}, 
            {"data": "monto_total"},
            {"data": "reg"}, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='<button type="button" class="btn btn-info" onclick="productoscompras('+row.compraId+')">Productos</button>\
                        <a class="btn btn-default" href="'+base_url+'Reportes/ticket_compras/'+row.compraId+'" target="_blank" title="Ticket"><i class="fa fa-ticket"></i></a>';
                    return html;
                }
            }, 
        ],
        order: [
            [0, "desc"]
        ],
    });
}
function productoscompras(id){
  $('#modal_productos').modal();
  var base_url = $('#base_url').val();
  $.ajax({
      type: 'POST',
      url: base_url + 'Compras/compraproductos',
      data: {
          compraid: id
      },
      async: false,
      statusCode: {
          404: function(data) {
              new PNotify({
                  title: 'Error!',
                  text: 'No Se encuentra el archivo',
                  type: 'error',
                  styling: 'bootstrap3'
              });
          },
          500: function() {
              new PNotify({
                  title: 'Error!',
                  text: 'Error 500',
                  type: 'error',
                  styling: 'bootstrap3'
              });
          }
      },
      success: function(data) {
        $('.viewproductos').html('');
        $('.viewproductos').html(data);
        $('#tableproductoscompras').DataTable({
          "lengthChange": false
        });
      }
  });
}